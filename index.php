<?php 
include_once ("inc/config.php");


?>
<!DOCTYPE html>
<!--[if lt IE 7]><html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]><html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]> <html class="no-js lt-ie9"><![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js" lang="fr"> <!--<![endif]-->
<!--[if lt IE 9]><script src="//html5shiv.googlecode.com/svn/trunk/html5.js"></script><![endif]-->
<html lang="fr">

	<head>
		<meta charset="utf-8" />
		<title>Loua - Brigade Anti-Poils</title>

		<meta http-equiv="content-type" content="text/html; charset=utf-8" />
		<meta name="description" content="LOUA, c’est la nouvelle Brigade Anti-Poils qui va contribuer à la lutte contre la sur-pilosité qui sévit sur notre planète ! Notre mission : punir les délinquants pileux multi récidivistes..." />
		<meta name="viewport" content="initial-scale=1,minimum-scale=1,width=device-width">

		<!-- Open Graph facebook -->
		<meta property="og:url" content="http://loua.fr"/>
		<meta property="og:title" content="Loua - La peau lisse"/> 
		<meta property="og:type" content="website"/> 
		<meta property="og:image" content="http://loua.fr/img/la-loua.jpg"/> 
		<meta property="og:site_name" content="LOUA.fr"/> 
		<meta property="og:description" content="LOUA, c’est la nouvelle Brigade Anti-Poils qui va contribuer à la lutte contre la sur-pilosité qui sévit sur notre planète ! Notre mission : punir les délinquants pileux multi récidivistes..."/>
		<meta name="viewport" content="initial-scale=1,minimum-scale=1,width=device-width">

		<?php include_once("header.php"); ?>

			<div class="content home">
				<div class="cols-row">
					<div class="col-66">
						<a href="<?php echo _INSTDIR_; ?>nos-bandes/bande-cire-froide-jambes">
							<img class="rounded" src="<?php echo _INSTDIR_; ?>img/h-slider.jpg" alt="">
						</a>
					</div>
					<div class="col-33">
						<a href="<?php echo _INSTDIR_; ?>jeux">
							<img class="rounded" src="<?php echo _INSTDIR_; ?>img/h-jeux.jpg" alt="">
						</a>
					</div>

				</div>

				<div class="cols-row">
					<div class="col-40">
						<a href="<?php echo _INSTDIR_; ?>videos">
							<img class="h-video" src="<?php echo _INSTDIR_; ?>img/h-video.jpg" alt="">
						</a>
					</div>
					<div class="col-60">
						<a href="<?php echo _INSTDIR_; ?>most-wanted/poil-dru">
							<img class="rounded" src="<?php echo _INSTDIR_; ?>img/h-poils.jpg" alt="">
						</a>
					</div>

				</div>

				<div class="cols-row">
					<div class="col-33">
						<a href="<?php echo _INSTDIR_; ?>connaitre-la-loua">
							<img class="rounded" src="<?php echo _INSTDIR_; ?>img/h-peau-lisse.jpg" alt="">
						</a>
					</div>
					<div class="col-66">
						<a href="<?php echo _INSTDIR_; ?>jeux">
							<img class="rounded" src="<?php echo _INSTDIR_; ?>img/h-concours.jpg" alt="">
						</a>
					</div>

				</div>
				
				<?php include_once("footer.php"); ?>
			</div>
		</div>
		
		<!-- <script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script> -->
		<script src="<?php echo _INSTDIR_; ?>js/jquery.min.js"></script>
		<script src="<?php echo _INSTDIR_; ?>js/main.js"></script>

	</body>
</html>