<?php 
require_once ("inc/config.php");
require_once ("inc/class.contest.php");
require_once ("inc/functions.php");

$contest = new Contest();

$page = 0;

if (isset($_GET['page'])){
	$page = $_GET['page'];
}

// Get all legfies
$leggfies = $contest->displayGalery($page);

$count = $contest->countLegfies();
?>
<!DOCTYPE html>
<!--[if lt IE 7]><html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]><html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]> <html class="no-js lt-ie9"><![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js" lang="fr"> <!--<![endif]-->
<!--[if lt IE 9]><script src="//html5shiv.googlecode.com/svn/trunk/html5.js"></script><![endif]-->
<html lang="fr">

	<head>
		<meta charset="utf-8" />
		<title>Galerie des Legfies</title>

		<meta http-equiv="content-type" content="text/html; charset=utf-8" />
		<meta name="description" content="Tous les legfies des participant au jeux concours Loua sont dans cette galerie. Pensez à voter pour votre legfie préféré." />
		<meta name="viewport" content="initial-scale=1,minimum-scale=1,width=device-width">

		<!-- Open Graph facebook -->
		<meta property="og:url" content="http://"/>
		<meta property="og:title" content=""/> 
		<meta property="og:type" content="website"/> 
		<meta property="og:image" content="http://"/> 
		<meta property="og:site_name" content=""/> 
		<meta property="og:description" content=""/>
		<meta name="viewport" content="initial-scale=1,minimum-scale=1,width=device-width">

		<?php include_once("header.php"); ?>

			<div class="content">
				<div class="galery-titre">
					<img src="<?php echo _INSTDIR_; ?>img/galery-titre.png" alt=""> 

					<?php if (!isset($_SESSION['loginId'])){ ?>
						<a class="btn-form" href="<?php echo _INSTDIR_; ?>login">Connexion</a>	
					<?php } else {?>
					<a class="btn-form" href="<?php echo _INSTDIR_; ?>post-legfie">Poste ton Legfie</a>
					<?php }?>
				</div>
				<div class="cols-row">
					<div class="col-100">
						<ul class="galery legfie-img">
						<?php foreach ($leggfies as $legfie): ?>
							<li>
								<a href="<?php echo _INSTDIR_."legfies/".$legfie->media_id ?>">
									<img src="<?php echo _INSTDIR_.$legfie->media_url ?>" alt="">		
									<span class="icon-heart"><?php echo $legfie->media_likes ?></span>
								</a>
							</li>
						<?php endforeach; ?>
						</ul>		
					</div>
				</div>

				<div class="cols-row">
					<div class="col-80 col-centre centered">
						<ul class="pagination">
							
							<li class="<?php if ($page == 0){ echo "current";} ?>"><a href="<?php echo _INSTDIR_?>legfies">1</a></li>
							<?php 
								$nbPage = (int)($count/12)+1;
								for ($i=2; $i <= $nbPage; $i++) { 
							?>
								<li class="<?php if ($page == $i){ echo "current";} ?>"><a href="<?php echo _INSTDIR_."legfies-" .$i; ?>"><?php echo $i; ?></a></li>
							 <?php } ?>
						</ul>
					</div>
				</div>


				<?php include_once("footer.php"); ?>
			</div>
			
		</div>
		
		<!-- <script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script> -->
		<script src="<?php echo _INSTDIR_; ?>js/jquery.min.js"></script>
		<script src="<?php echo _INSTDIR_; ?>js/main.js"></script>

	</body>
</html>