<?php 
include_once ("inc/config.php");


?>
<!DOCTYPE html>
<!--[if lt IE 7]><html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]><html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]> <html class="no-js lt-ie9"><![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js" lang="fr"> <!--<![endif]-->
<!--[if lt IE 9]><script src="//html5shiv.googlecode.com/svn/trunk/html5.js"></script><![endif]-->
<html lang="fr">

	<head>
		<meta charset="utf-8" />
		<title>Réglements du Jeux Concours</title>

		<meta http-equiv="content-type" content="text/html; charset=utf-8" />
		<meta name="description" content="Règles pour participez au Jeux Concours legfie de LOUA." />
		<meta name="viewport" content="initial-scale=1,minimum-scale=1,width=device-width">

		<!-- Open Graph facebook -->
		<meta property="og:url" content="http://"/>
		<meta property="og:title" content=""/> 
		<meta property="og:type" content="website"/> 
		<meta property="og:image" content="http://"/> 
		<meta property="og:site_name" content=""/> 
		<meta property="og:description" content=""/>
		<meta name="viewport" content="initial-scale=1,minimum-scale=1,width=device-width">

		<?php include_once("header.php"); ?>

			<div class="content">
				<div class="cols-row respect-content">
					<div class="cols-row begin end">
						<div class="col-66 col-centre centered">
							<h1 class="upper">Réglement du jeux Concours</h1>
						</div>
					</div>
					<div class="cols-row">
						<div class="col-100">
							<h2>Article 1 : Organisation</h2>
							<p>LAURENCE DUMONT SA, ci-après désignée sous le nom « L'organisatrice », dont le siège social est situé à Allée de Saylat, Zac Agropole, CS20044, 47901 Agen Cedex 9, immatriculée sous le numéro AGEN RCS 398 063 065, organise un jeu gratuit sans obligation d'achat du 03/07/2015 au 06/09/2015 minuit (jour inclus).</p>

							<h2>Article 2 : Participants </h2>
							<p>Ce jeu gratuit sans obligation d'achat est exclusivement ouvert aux personnes majeures, à la date du début du jeu, résidant en France métropolitaine (Corse comprise). Sont exclues du jeu les personnes ne répondant pas aux conditions ci-dessus ainsi que les membres du personnel de « L'organisatrice », et toute personne ayant directement ou indirectement participé à la conception, à la réalisation ou à la gestion du jeu ainsi que leur conjoint, les membres de leurs familles : ascendants et descendants directs ou autres parents vivants ou non sous leur toit. « L'organisatrice » se réserve le droit de demander à tout participant de justifier des conditions ci-dessus exposées. Toute personne ne remplissant pas ces conditions ou refusant de les justifier sera exclue du jeu et ne pourra, en cas de gain, bénéficier de son lot. Il n'est autorisé qu'une seule participation par personne (même nom, même adresse). « L'organisatrice » se réserve le droit de procéder à toute vérification pour le respect de cette règle. La participation au jeu implique l'entière acceptation du présent règlement. </p>

							<h2>Article 3 : Modalités de participation </h2>
							<p>La participation au Jeu se fait exclusivement par voie électronique. A ce titre, toute inscription par téléphone, télécopie, courrier postal ou courrier électronique ne pourra être prise en compte. </p>
							<p>Pour participer, le participant est invité à suivre les étapes suivantes :
								<ol>
									<li>se rendre sur le site internet www.loua.fr  du 03 juillet 2015 au 6 septembre 2015 inclus.</li>
									<li>s’inscrire sur le site en renseignant tous les champs du formulaire nécessaires à son inscription, puis confirmer qu’il souhaite participer au Jeu,</li>
									<li>accepter le règlement complet du jeu après l’avoir lu</li>
								</ol>
							</p>

							<p>Le participant doit remplir totalement et correctement le formulaire de renseignements pour que son inscription soit validée. Le joueur est informé et accepte que les informations saisies dans le formulaire d'inscription vaillent preuve de son identité. Toute participation effectuée contrairement aux dispositions du présent règlement rendra la participation invalide. Tout participant suspecté de fraude pourra être écarté du jeu-concours par « L'organisatrice » sans que celle-ci n'ait à en justifier. Toute identification ou participation incomplète ou erronée, volontairement ou non, ou réalisée sous une autre forme que celle prévue dans le présent règlement sera considérée comme nulle. La même sanction s'appliquera en cas de multi-participation. </p>

							<h2>Article 4 : Principe du Jeu et détermination des gagnants</h2>

							<p>
								<ul>
									<li><strong>Le tirage au sort du voyage pour 2 personnes :</strong> <br> Pour s’inscrire au tirage au sort final, le participant devra poster un Legfie (une photographie de ses jambes),  sur le site  www.loua.fr  pendant la période du 03/07/2015 au 06/09/2015 minuit (jour inclus). Le gagnant du Jeu sera déterminé par un tirage au sort sous le contrôle d’un huissier, parmi l'ensemble des personnes ayant posté un Legfie. Seules les personnes qui auront rempli correctement le formulaire d’inscription pourront participer au tirage au sort. </li>
									<li><strong>Les 50 Legfies ayant obtenus le plus de votes :</strong><br> Pour s’inscrire le participant devra poster un Legfie (une photographie de ses jambes),  sur le site www.loua.fr pendant la période du 03/07/2015 au 06/09/2015 minuit (jour inclus). Une fois sa photographie mise en ligne sur le site, le participant pourra partager son Legfie sur les réseaux sociaux afin de récolter un maximum de votes. Les personnes souhaitant voter pour les Legfies auront droit à un seul vote toutes les 24 heures et devront remplir le formulaire d’inscription. Les 50 gagnants seront déterminés par le nombre de votes récoltés à la fin du Jeu. Seules les personnes qui auront rempli correctement le formulaire d’inscription pourront participer au Jeu. </li>
								</ul>
							</p>
							<p>Le tirage au sort et l’élection des 50 Legfies ayant récoltés le plus de votes seront réalisés le 07/09/2015.</p>

							<h2>Article 5 : Les dotations mises en jeu sont réparties comme suit</h2>

							<p>
								<ul>
									<li><strong>Gagnant du tirage au sort : </strong><br>1 séjour de 1 semaine pour 2 personnes en République Dominicaine en formule ALL INCLUSIVE dans un hôtel 3*, d’une valeur totale de 1800€ TTC*. <br> <small>*Vol au départ de Paris. Séjour à réserver entre le 01/02/2016 et le 30/04/2016. La personne tirée au sort devra indiquer ses dates de voyage à minima 90 jours avant le départ.</small></li>
									<li><strong>Gagnants des 50 meilleurs Legfies qui ont obtenu le plus de votes : </strong>
										<p><ul>
											<li>Du 1er au 3e gagnant : 3 Fujifilm Instax Mini 8 d’une valeur de 79€ unitaire.</li>
											<li>Du 4e au 10e gagnant : 7 serviettes de plage ROXY collection Hazy d’une valeur de 39€95 unitaire. Dimensions 160 cm x 90 cm 100% coton.</li>
											<li>Du 11e au 20e gagnant : 10 sacs de plage en paille ROXY collection Sun Keeper d’une valeur de 29€95 unitaire. Dimensions : 35,6 x 58,4 x 38,1 cm.</li>
											<li>Du 21e au 30e gagnant : 10 chapeaux de paille ROXY modèle COWGIRL d’une valeur de 29€95 unitaire. Taille S/M (56).</li>
											<li>Du 31e au 40e gagnant : 10 eaux de parfum Vanille Monoï Les Senteurs Gourmandes d’une valeur de 21€ unitaire.</li>
											<li>Du 41e au 50e gagnant : 10 gels douches parfumés Vanille Monoï Les Senteurs Gourmandes d’une valeur de 7€90 unitaire.</li>
										</ul></p>
										<p>La valeur des prix est déterminée au moment de la rédaction du présent règlement et ne saurait faire l'objet d'une contestation quant à leur évaluation.</p>
									</li>
								</ul>
							</p>

							<h2>Article 6 : Annonce des gagnants</h2>

							<p>Les gagnants seront informés de leur gain par « L'organisatrice » par e-mail sous 2 semaines à partir du 07/09/2015 et auront 15 jours pour valider et confirmer par e-mail leurs coordonnées, afin de permettre un bon acheminement de la dotation. L’absence de réponse dans ce délai de 15 jours entraînera la perte de la dotation, et aucun nouveau gagnant ne sera désigné pour se substituer au(x) gagnant(s) n’ayant pas confirmé leurs coordonnées.</p>
							<p>A tout moment, le participant est responsable de l’exactitude des informations qu’il a communiquées. Seront considérées comme nulles toutes les participations pour lesquelles les adresses emails ou coordonnées déposées sur le site www.loua.fr seraient incomplètes, erronées, ou manifestement incohérentes.</p>

							<p>La liste des gagnants sera disponible sur la page Facebook Laurence Dumont Cosmétiques <a href="https://www.facebook.com/LaurenceDumontCosmetiques">https://www.facebook.com/LaurenceDumontCosmetiques</a></p>

							<h2>Article 7 : Remise du lot : par envoi postal</h2>

							<p>Les gagnants s'engagent à accepter les lots tels que proposés sans possibilité d'échange notamment contre des espèces, d'autres biens ou services de quelque nature que ce soit ni transfert du bénéfice à une tierce personne. De même, ces lots ne pourront faire l'objet de demandes de compensation. «L'organisatrice » se réserve le droit, en cas de survenance d'un événement indépendant de sa volonté, notamment lié à ses fournisseurs ou à des circonstances imprévisibles, de remplacer les lots annoncés, par des lots de valeur équivalente. Les gagnants seront  tenu informés des éventuels changements.</p>

							<h2>Article 8 : Utilisation des données personnelles des participants</h2>

							<p>Les informations des participants sont enregistrées et utilisées par « L'organisatrice » pour mémoriser leur participation au jeu-concours et permettre l'attribution des lots. Les participants peuvent, pour des motifs légitimes, s'opposer à ce que leurs données personnelles communiquées dans le cadre de ce jeu fassent l'objet d'un traitement. Ils disposent également d'un droit d'opposition à ce qu'elles soient utilisées à des fins de prospection commerciale, en dehors de la participation à ce jeu-concours, qu'ils peuvent faire valoir dès l'enregistrement de leur participation en s'adressant par courrier à « L'organisatrice » dont l'adresse est mentionnée à l'article 1.</p>
							<p>Les gagnants  autorisent « L'organisatrice » à utiliser à titre publicitaire ou de relations publiques leurs coordonnées (nom, prénom), sur quelque support que ce soit, sans que cela ne leur confère une rémunération, un droit ou un avantage quelconque, autre que l'attribution de leur lot. Conformément à la Loi Informatique et Libertés du 6 janvier 1978, tout participant a le droit d'exiger que soient rectifiées, complétées, clarifiées, mises à jour ou effacées, les informations le concernant qui seraient inexactes, incomplètes, équivoques ou périmées en s'adressant par courrier à « L'organisatrice » dont l'adresse est mentionnée à l'article 1. </p>

							<h2>Article 9 : Remboursement des frais de connexion liés au jeu</h2>

							<p>Conformément aux dispositions de l'article L. 121-36 du Code de la consommation, l'accès au site internet et la participation au jeu qui y est proposé sont entièrement libres et gratuits, en sorte que les frais de connexion au site, exposés par le participant, lui seront remboursés selon les modalités ci-dessous : </p>
							<p>- Un seul remboursement par mois et par foyer (même nom, même adresse postale) – </p>
							<p>Participant résidant en France- Durée maximum de connexion permettant de participer au jeu de 5 minutes. Les fournisseurs d'accès à Internet offrant actuellement, une connexion gratuite ou forfaitaire aux internautes, il est expressément convenu que tout accès au site s'effectuant sur une base gratuite ou forfaitaire ne pourra donner lieu à aucun remboursement, dans la mesure où l'abonnement aux services du fournisseur d'accès est dans ce cas contracté par l'internaute pour son usage de l'Internet en général et que le fait pour le participant de se connecter au site et de participer au jeu ne lui occasionne aucun frais ou débours supplémentaire. Les frais de connexion seront remboursés en cas de connexion payante facturée au prorata de la durée de communication. Dans l'hypothèse d'une connexion faisant l'objet d'un paiement forfaitaire pour une durée déterminée et, au-delà de cette durée, facturée au prorata de la durée de communication, les frais de connexion au site seront remboursés au participant dès lors qu'il est établi que le participant a excédé le forfait dont il disposait et que ce forfait a été dépassé du fait de la connexion au site. Pour obtenir le remboursement de ses frais de connexion, ainsi que des frais d'affranchissement de sa demande de remboursement, le participant doit adresser à « L'organisatrice », dans le mois du débours de ces frais, le cachet de la poste faisant foi, une demande écrite, établie sur papier libre, contenant les éléments suivants :- l'indication de ses noms, prénom et adresse postale personnelle - l'indication des dates, des heures et des durées de ses connexions au site- la copie de la facture détaillée de l'opérateur téléphonique et/ou du fournisseur d'accès auquel il est abonné, faisant apparaître les dates et heures de ses connexions au site. Les frais d'affranchissement nécessaires à la demande de remboursement des frais de connexion seront remboursés, sur demande, sur la base du tarif postal lent en vigueur. Les frais de connexion sur le site pour la participation au jeu seront remboursés par chèque dans les deux mois de la réception de la demande du participant. </p>

							<h2>Article 10 : Règlement du jeu</h2>
							<p>Le règlement pourra être consulté sur le site suivant : www.loua.fr</p>
							<p>Il peut être adressé à titre gratuit (timbre remboursé sur simple demande), à toute personne qui en fait la demande auprès de « L'organisatrice ». « L'organisatrice » se réserve le droit de prolonger, écourter, modifier ou annuler le jeu à tout moment, notamment en cas de force majeure, sans qu'il puisse être prétendu à aucune indemnité par les participants.</p>

							<h2>Article 11 : Propriété industrielle et intellectuelle</h2>

							<p>La reproduction, la représentation ou l'exploitation de tout ou partie des éléments composant le jeu, le présent règlement compris sont strictement interdites. Toutes les marques, logos, textes, images, vidéos et autres signes distinctifs reproduits sur le site ainsi que sur les sites auxquels celui-ci permet l'accès par l'intermédiaire de liens hypertextes, sont la propriété exclusive de leurs titulaires et sont protégés à ce titre par les dispositions du Code de la propriété intellectuelle et ce pour le monde entier. Leur reproduction non autorisée constitue une contrefaçon passible de sanctions pénales. Toute reproduction, totale ou partielle, non autorisée de ces marques, logos et signes constitue une contrefaçon passible de sanctions pénales. La participation à ce jeu implique l'acceptation pleine et entière du présent règlement par les participants. </p>

							<h2>Article 12 : Responsabilité</h2>

							<p>La responsabilité de « L'organisatrice » ne saurait être engagée en cas de force majeure ou de cas fortuit indépendant de sa volonté. « L'organisatrice » ne saurait être tenue pour responsable des retards, pertes, vols, avaries des courriers, manque de lisibilité des cachets du fait des services postaux. Elle ne saurait non plus être tenue pour responsable et aucun recours ne pourra être engagé contre elle en cas de survenance d'événements présentant les caractères de force majeure (grèves, intempéries...) privant partiellement ou totalement les participants de la possibilité de participer au jeu et/ou les gagnants du bénéfice de leurs gains. « L'organisatrice » ainsi que ses prestataires et partenaires ne pourront en aucun cas être tenus pour responsables des éventuels incidents pouvant intervenir dans l'utilisation des dotations par les bénéficiaires ou leurs invités dès lors que les gagnants en auront pris possession. De même « L'organisatrice », ainsi que ses prestataires et partenaires, ne pourront être tenus pour responsables de la perte ou du vol des dotations par les bénéficiaires dès lors que les gagnants en auront pris possession. Tout coût additionnel nécessaire à la prise en possession des dotations est à l'entière charge des gagnants sans que ceux-ci ne puissent demander une quelconque compensation à « L'organisatrice », ni aux sociétés prestataires ou partenaires. Ce jeu-concours n'est pas géré ou parrainé par Facebook que « L'organisatrice » décharge de toute responsabilité. </p>

							<h2>Article 13 : Litige & Réclamation</h2>

							<p>Toute difficulté qui viendrait à naître de l’application ou de l’interprétation du présent règlement sera tranchée par la Société Organisatrice qui statuera souverainement et de manière définitive sans possibilité de recours. Toute contestation relative au présent Jeu devra être adressée, par courrier postal uniquement à l’adresse du Jeu visée à l’article 1. Les frais postaux relatifs à cet envoi seront à la charge du plaignant. Aucune contestation ou réclamation ne sera prise en considération après un délai d'un mois suivant la date de clôture du Jeu.</p>

							<h2>Article 14 : Convention de preuve </h2>

							<p>De convention expresse entre le participant et « L'organisatrice », les systèmes et fichiers informatiques de « L'organisatrice » feront seuls foi. Les registres informatisés, conservés dans les systèmes informatiques de « L'organisatrice », dans des conditions raisonnables de sécurité et de fiabilité, sont considérés comme les preuves des relations et communications intervenues entre « L'organisatrice » et le participant. Il est en conséquence convenu que, sauf erreur manifeste, « L'organisatrice » pourra se prévaloir, notamment aux fins de preuve de tout acte, fait ou omission, des programmes, données, fichiers, enregistrements, opérations et autres éléments (tels que des rapports de suivi ou autres états) de nature ou sous format ou support informatiques ou électroniques, établis, reçus ou conservés directement ou indirectement par « L'organisatrice », notamment dans ses systèmes informatiques. Les éléments considérés constituent ainsi des preuves et s'ils sont produits comme moyens de preuve par « L'organisatrice » dans toute procédure contentieuse ou autre, ils seront recevables, valables et opposables entre les parties de la même manière, dans les mêmes conditions et avec la même force probante que tout document qui serait établi, reçu ou conservé par écrit. Les opérations de toute nature réalisées à l'aide de l'identifiant et du code attribués à un participant, à la suite de l'inscription, sont présumées de manière irréfragable, avoir été réalisées sous la responsabilité du participant.</p>

						</div>

					</div>
					

				</div>
			
				<?php include_once("footer.php"); ?>
			</div>
			
		</div>
		
		<!-- <script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script> -->
		<script src="js/jquery.min.js"></script>
		<script src="js/main.js"></script>

	</body>
</html>