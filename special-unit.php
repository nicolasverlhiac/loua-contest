<?php 
include_once ("inc/config.php");

$key = $_GET["key"];


?>
<!DOCTYPE html>
<!--[if lt IE 7]><html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]><html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]> <html class="no-js lt-ie9"><![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js" lang="fr"> <!--<![endif]-->
<!--[if lt IE 9]><script src="//html5shiv.googlecode.com/svn/trunk/html5.js"></script><![endif]-->
<html lang="fr">

	<head>
		<meta charset="utf-8" />
		<title>Nos Unités Spéciales</title>

		<meta http-equiv="content-type" content="text/html; charset=utf-8" />
		<meta name="description" content="Découvrez les unités spéciales de la brigade épilation de Laurence Dumont. Haute tolérance, Les dermo Soins, For Men et Épilation Institut." />
		<meta name="viewport" content="initial-scale=1,minimum-scale=1,width=device-width">

		<!-- Open Graph facebook -->
		<meta property="og:url" content="http://"/>
		<meta property="og:title" content=""/> 
		<meta property="og:type" content="website"/> 
		<meta property="og:image" content="http://"/> 
		<meta property="og:site_name" content=""/> 
		<meta property="og:description" content=""/>
		<meta name="viewport" content="initial-scale=1,minimum-scale=1,width=device-width">

		<?php include_once("header.php"); ?>

			<div class="content">
				<div class="menu-bandes menu-ld rounded cols-row">
					<div class="col-90 cols-split col-centre">
						<div class="cols-row end begin">
							<div class="col-30">
								<a href="<?php echo _INSTDIR_; ?>nos-unites-speciales/laurence-dumont-institut">
									<img src="<?php echo _INSTDIR_; ?>img/ld-institut.png" alt="">
								</a>
							</div>
							<div class="col-20">
								<a href="<?php echo _INSTDIR_; ?>nos-unites-speciales/for-men">
									<img src="<?php echo _INSTDIR_; ?>img/ld-men.png" alt="">
								</a>
							</div>
							<div class="col-30">
								<a href="<?php echo _INSTDIR_; ?>nos-unites-speciales/haute-tolerance">
									<img src="<?php echo _INSTDIR_; ?>img/ld-tolerance.png" alt="">
								</a>
							</div>
							<div class="col-20">
								<a href="<?php echo _INSTDIR_; ?>nos-unites-speciales/les-dermos-soins">
									<img src="<?php echo _INSTDIR_; ?>img/ld-dermo.png" alt="">
								</a>
							</div>	
						</div>
					</div>
				</div>
				<br>
				<div class="cols-row">
					<div class="col-100 bande-content ld-content">

						<?php include_once("ld/$key.php") ; ?>
					</div>

				</div>

				<?php include_once("footer.php"); ?>
			</div>
			
		</div>
		
		<!-- <script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script> -->
		<script src="<?php echo _INSTDIR_; ?>js/jquery.min.js"></script>
		<script src="<?php echo _INSTDIR_; ?>js/main.js"></script>

	</body>
</html>