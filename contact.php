<?php 
include_once ("inc/config.php");


?>
<!DOCTYPE html>
<!--[if lt IE 7]><html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]><html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]> <html class="no-js lt-ie9"><![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js" lang="fr"> <!--<![endif]-->
<!--[if lt IE 9]><script src="//html5shiv.googlecode.com/svn/trunk/html5.js"></script><![endif]-->
<html lang="fr">

	<head>
		<meta charset="utf-8" />
		<title>Contactez la Peau Lisse</title>

		<meta http-equiv="content-type" content="text/html; charset=utf-8" />
		<meta name="description" content="Informations de Contact pour les participants au jeux concours ou pour plus d'informations" />
		<meta name="viewport" content="initial-scale=1,minimum-scale=1,width=device-width">

		<!-- Open Graph facebook -->
		<meta property="og:url" content="http://"/>
		<meta property="og:title" content=""/> 
		<meta property="og:type" content="website"/> 
		<meta property="og:image" content="http://"/> 
		<meta property="og:site_name" content=""/> 
		<meta property="og:description" content=""/>
		<meta name="viewport" content="initial-scale=1,minimum-scale=1,width=device-width">

		<?php include_once("header.php"); ?>

			<div class="content">
				<div class="cols-row respect-content">
					<div class="cols-row">
						<div class="col-100">
							<h2>Nous contacter</h2>
							<p>Laboratoire Laurence Dumont</p>
							<p>Allée du saylat - ZAC Agropole </p>
							<p>BP20044 - Estillac </p>
							<p>47901 Agen cedex 9 </p>
							<p>tél: +33(0)5.53.95.92.19</p>
							<p>fax: +33(0)5.53.95.74.90</p>

						</div>
					</div>
				</div>
			
				<?php include_once("footer.php"); ?>
			</div>
			
		</div>
		
		<!-- <script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script> -->
		<script src="js/jquery.min.js"></script>
		<script src="js/main.js"></script>

	</body>
</html>