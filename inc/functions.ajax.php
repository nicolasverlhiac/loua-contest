<?php
include_once("config.php");
require_once ("class.contest.php");
require_once ("functions.php");

$action = $_REQUEST["action"];

/**
 * ADD - Adding New User
 **/

if ($action == "addUser") {

    $candidat_pseudo = trim(stripslashes($_POST['candidat_pseudo']));
    $candidat_mail = trim(strtolower($_POST['candidat_mail']));
    $candidat_password = trim(stripslashes($_POST['candidat_password']));
    $candidat_type = trim(stripslashes($_POST['candidat_type']));
    $candidat_ip = $_SERVER['REMOTE_ADDR'];
    $candidat_date = date('Y-m-d H:i:s');  // Format date de MySQL

    $contest = new Contest();

    $error = array();
    if(!$candidat_pseudo) $error[] = "Il manque le pseudo";
    if($contest->existPseudo($candidat_pseudo)) $error[] = "Le pseudo existe deja";
    
    if(!$candidat_mail) $error[] = "Il manque l'e-mail";
    elseif(!is_email($candidat_mail)) $error[] = "Mauvais e-mail";
    if($contest->existUser($candidat_mail)) $error[] = "E-mail deja existant";
    
    if(!$candidat_password) $error[] = "Il manque le mot de passe";
    
    if(!$candidat_type) $error[] = "Il manque le sexe de la personne";
    
    if(!$candidat_date) $error[] = "Il manque la date";
    
    if(!$error) {

        // Create new user
        $contest->createUser($candidat_pseudo, $candidat_mail, $candidat_password, $candidat_type, $candidat_ip, $candidat_date);

        // On log l'utilisateur et on se casse
        $contest->login($candidat_mail, $candidat_password);

        if(!$in_site) exit("OK");

    } else {

        $error = implode(" <br> ",$error);
        if (!$in_site) echo $error;

    }
}

/**
 * LOGIN - User login
 **/

if ($action == "login") {

    $candidat_mail = trim(strtolower($_POST['candidat_mail']));
    $candidat_password = trim(stripslashes($_POST['candidat_password']));

    $contest = new Contest();

    $error = array();
    if(!$candidat_mail) $error[] = "Il manque e-mail / Username";
    elseif(!$contest->existUser($candidat_mail)) $error[] = "Email incorrecte";
    if(!$candidat_password) $error[] = "Il manque le mot de passe";
    elseif(!$contest->existLogin($candidat_mail, $candidat_password)) $error[] = "Mauvais mot de passe";

    if(!$error) {

      // On log l'utilisateur et on se casse
      $contest->login($candidat_mail, $candidat_password);

      if(!$in_site) exit("OK");

    } else {
      $error = implode(" <br> ",$error);
      if (!$in_site) echo $error;
    }
}


/**
 * ADD - Legfie
 **/

if ($action == "addLegfie") {

    $candidat_id = $_SESSION['loginId'];

    $candidat_nom = trim(stripslashes($_POST['candidat_nom']));
    $candidat_prenom = trim(stripslashes($_POST['candidat_prenom']));
    $candidat_codepostal = trim(stripslashes($_POST['candidat_codepostal']));
    $candidat_birthday = trim(stripslashes($_POST['candidat_birthday']));
    $media_description = trim(stripslashes($_POST['media_description']));
    $media_url = trim(stripslashes($_POST['media_url']));
    $media_date = date('Y-m-d H:i:s');  // Format date de MySQL

    $contest = new Contest();

    $error = array();
    if(!$candidat_id) $error[] = "Vous n'êtes pas connecté" ;
    if(!$candidat_nom) $error[] = "Il manque ton nom";
    if(!$candidat_prenom) $error[] = "Il manque ton prenom";
    if(!$candidat_birthday) $error[] = "Il manque l'année de naissance";
    if(!$candidat_codepostal) $error[] = "Il manque ton code postal";
    if(!$media_url) $error[] = "Il manque la photo de ton Legfie ";
    elseif ($media_url == "/img/legfie.jpg") $error[] = "Il manque la photo de ton Legfie ";
    if(!$media_date) $error[] = "Il manque la date";

    if($contest->existLegfie($candidat_id)) $error[] = "<strong>Vous ne pouvez candidater qu'une seule fois.</strong>";

    if(!$error) {

        // Add user's information
        $contest->addLegfieInfo($candidat_nom, $candidat_prenom, $candidat_codepostal, $candidat_id, $candidat_birthday);

        // Add Legfie in media database
        $contest->createLegfieMedia($candidat_id, $media_description, $media_url ,$media_date);

        if(!$in_site) exit("OK");

    } else {
      $error = implode(" <br> ",$error);
      if (!$in_site) echo $error;
    }
}



/**
 * ADD - Vote
 **/

if ($action == "vote") {

    $candidat_id = $_SESSION['loginId'];
    $media_id = trim(stripslashes($_POST['media_id']));
    $like_registered = date('Y-m-d H:i:s');  // Format date de MySQL

    $contest = new Contest();

    $error = array();
    // if() $error[] = "Vous avez déjà voté";
    $chekLike = $contest->checkLike($candidat_id);

    // Date now - Date in database (last vote)
    $timeVote = strtotime($like_registered) - strtotime($chekLike[0]['like_registered']);

    // Calcul time for Human

    $timeRest = 86400 - $timeVote;
    $heures=intval($timeRest / 3600);
    $minutes=intval(($timeRest % 3600) / 60);
    $secondes=intval((($timeRest % 3600) % 60));

    $timeHuman = $heures.' h '.$minutes.' min '.$secondes.' sec';

    if($timeVote < 86400) $error[] = "Un vote par 24h (il reste <strong>".$timeHuman."</strong>)";

    if(!$error) {

        $leggfie = $contest->displayLegfie($media_id);
        $media_likes = $leggfie[0]["media_likes"] + 1;

        // Add user's information
        $contest->addLike($media_id, $media_likes);
        
        $contest->createLike($media_id, $candidat_id, $like_registered);

        if(!$in_site) exit("OK");

    } else {
      $error = implode(" <br> ",$error);
      if (!$in_site) echo $error;
    }
}