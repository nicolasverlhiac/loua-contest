<?php

class Contest{

	// Private ORM instance
	private $orm;

	/**
	 * Add New User in database
	 * @param string $candidat_pseudo
	 * @param string $candidat_mail
	 * @param string $candidat_password (hash)
	 * @param int    $candidat_type [1 = Man | 2 = Women]
	 * @param int    $candidat_ip
	 * @param date   $candidat_date
	 * @return Save
	 */

	public static function createUser($candidat_pseudo, $candidat_mail, $candidat_password, $candidat_type, $candidat_ip, $candidat_date){

		// Hash Password to protect it
		$candidat_password = hashPassword($candidat_password);
		
		// Inscrit un nouveau film dans la base de données
		$result = ORM::for_table('users')->create();
		$result->user_login = $candidat_pseudo;
		$result->user_email = $candidat_mail;
		$result->user_pass = $candidat_password;
		$result->user_gender = $candidat_type;
		$result->user_ip = $candidat_ip;
		$result->user_registered = $candidat_date;
		$result->user_status = "0"; // Waiting validation [0 = wait | 1 = active | 2 = admin]

		$result->save();

	}

	/**
	 * Add New Legfie Informtion (Update personal information's user)
	 * @param string $candidat_nom
	 * @param string $candidat_prenom
	 * @param string $candidat_codepostal
	 * @param int    $candidat_id
	 * @return Save
	 */


	public static function addLegfieInfo($candidat_nom, $candidat_prenom, $candidat_codepostal, $candidat_id, $candidat_birthday){

		$result = ORM::for_table('users')->find_one($candidat_id);
		$result->set(array(
		    'user_name' => $candidat_nom,
		    'user_prenom' => $candidat_prenom,
		    'user_codepostal' => $candidat_codepostal,
		    'user_birthday' => $candidat_birthday
		));
		$result->save();

	}


	public static function displayLegfie($media_id){

			$result = ORM::for_table('medias')
						->raw_query('SELECT * FROM `medias` WHERE media_id = '.$media_id)
						->find_many();
			return $result;
	}

	public static function displayLegfieInfo($user_id){

			$result = ORM::for_table('users')
						->raw_query('SELECT * FROM `users` WHERE user_id = '.$user_id)
						->find_many();
			return $result;
	}

	public static function checkLike($candidat_id){

		// Does the user e-mail exist in the database?
		$result = ORM::for_table('likes')
					->raw_query('SELECT * FROM `likes` WHERE user_id = '.$candidat_id.' ORDER BY like_registered DESC')
					->find_many();

		return $result;
	}

	public static function  addLike($media_id, $media_likes){

		$result = ORM::for_table('medias')->find_one($media_id);
		$result->set(array(
		    'media_likes' => $media_likes
		));
		$result->save();

	}

	public static function  createLike($media_id, $user_id, $like_registered){

		$result = ORM::for_table('likes')->create();
		$result->user_id = $user_id;
		$result->media_id = $media_id;
		$result->like_registered = $like_registered;

		$result->save();

	}


	public static function displayGalery($page){

			if ($page > 0) {
				$page = $page - 1;
			}
			$page = $page * 12;


			$result = ORM::for_table('medias')
						->raw_query('SELECT * FROM `medias` WHERE media_status = 1 ORDER BY media_id DESC LIMIT '.$page.' , 12')
						->find_many();
			return $result;
	}


	public static function countLegfies(){

		$result = ORM::for_table('medias')->count();
		return $result;
	}

	public static function displayLastLegfie(){

		$result = ORM::for_table('medias')
					->raw_query('SELECT * FROM `medias` WHERE media_status = 1 ORDER BY media_id DESC LIMIT 0 , 5')
					->find_many();
		return $result;
	}

	/**
	 * Add New Legfie Informtion (Update personal information's user)
	 * @param int    $candidat_id
	 * @param string $media_description
	 * @param string $media_url
	 * @param date   $media_date
	 * @return Save
	 */


	public static function createLegfieMedia($candidat_id, $media_description, $media_url ,$media_date){
		
		$result = ORM::for_table('medias')->create();
		$result->media_url = $media_url;
		$result->media_description = $media_description;
		$result->media_registered = $media_date;
		$result->media_status = "0"; // Waiting validation [0 = wait | 1 = active]
		$result->user_id = $candidat_id;

		$result->save();

	}

	/**
	 * Check whether such a Legfie has been already sent
	 * @param string $candidat_id
	 * @return boolean
	 */

	public static function existLegfie($candidat_id){

		// Does the user e-mail exist in the database?
		$result = ORM::for_table('medias')
					->where('user_id', $candidat_id)
					->count();

		return $result == 1;
	}


	/**
	 * Check whether such a user mail exists in the database and return a boolean.
	 * @param string $candidat_mail
	 * @return boolean
	 */

	public static function existUser($candidat_mail){

		// Does the user e-mail exist in the database?
		$result = ORM::for_table('users')
					->where('user_email', $candidat_mail)
					->count();

		return $result == 1;
	}

	/**
	 * Check whether such a user pseudo exists in the database and return a boolean.
	 * @param string $candidat_pseudo
	 * @return boolean
	 */

	public static function existPseudo($candidat_pseudo){

		// Does the login (pseudo) exist in the database?
		$result = ORM::for_table('users')
					->where('user_login', $candidat_pseudo)
					->count();

		return $result == 1;
	}


	/**
	 * Check whether such a user mail and user password exists in the database and return a boolean.
	 * @param string $candidat_mail
	 * @param string $candidat_password
	 * @return boolean
	 */

	public function existLogin($candidat_mail, $candidat_password){

		// On hash le mot de passe car on l'a anregistré hashé
		$candidat_password = hashPassword($candidat_password);
		
		$result = ORM::for_table('users')
					->where(array(
		                'user_email' => $candidat_mail,
		                'user_pass' => $candidat_password
		            ))
					->count();

		return $result == 1;
	}

	public function needId($userNameOrEmail, $motDePasse){

		// On hash le mot de passe car on l'a anregistré hashé
		$motDePasse = hashPassword($motDePasse);
		
		$utilisateurLog = ORM::for_table('Utilisateur')
					->where(array(
		                'utilisateur_username' => $userNameOrEmail,
		                'utilisateur_passe' => $motDePasse
		            ))
					->find_one();

		return $utilisateurLog->utilisateur_id;
	}

	public function login($candidat_mail, $candidat_password){

		// On hash le mot de passe car on l'a anregistré hashé
		$candidat_password = hashPassword($candidat_password);
		
		$utilisateurLog = ORM::for_table('users')
					->where(array(
		                'user_email' => $candidat_mail,
		                'user_pass' => $candidat_password
		            ))
					->find_one();

		$_SESSION['loginMail'] = $utilisateurLog->user_email;
		$_SESSION['loginPseudo'] = $utilisateurLog->user_login;
		$_SESSION['loginPass'] = $utilisateurLog->user_pass;
		$_SESSION['loginId'] = $utilisateurLog->user_id;
	}

	// ==================================
	// ADMINSITRATION
	// ==================================

	public static function displayWait(){

			$result = ORM::for_table('medias')
						->raw_query('SELECT * FROM `medias` WHERE media_status = 0 ORDER BY media_id DESC')
						->find_many();
			return $result;
	}

	public static function validateLegfieWait($id){
		
		$result = ORM::for_table('medias')->find_one($id);
		$result->set(array(
		    'media_status' => "1",
		));
		$result->save();
	}


	public static function displayMedia($user_id){

			$result = ORM::for_table('medias')
						->raw_query('SELECT * FROM `medias` WHERE user_id = '.$user_id)
						->find_many();
			return $result;
	}

	public static function deleteLegfieWait($id){
		
		$result = ORM::for_table('medias')->find_one($id);
		$result->set(array(
		    'media_status' => "2",
		));
		$result->save();
	}

	public static function countStatsLegfie(){
		
		$result = ORM::for_table('medias')
					->raw_query('SELECT * FROM `medias` WHERE media_status = 1')
					->find_many();
		return $result;
	}


	public static function displayWinner(){

			$result = ORM::for_table('medias')
						->raw_query('SELECT * FROM `medias` ORDER BY `media_likes` DESC LIMIT 0 , 50')
						->find_many();
			return $result;
	}


	public static function countFake($id_user,$ip_user,$id_media){


			$result = ORM::for_table('likes')
						->raw_query('SELECT l.media_id, l.user_id, u.user_email, l.like_id, u.user_ip, l.like_registered FROM likes l, users u WHERE l.media_id = :mediaID AND u.user_id = l.user_id AND u.user_ip = :userIP AND l.user_id != :userID ', array('userID' => $id_user, 'userIP' => $ip_user, 'mediaID' => $id_media))
						->find_many();
			return $result;
	}

	public static function selectFakeUsers($id_user,$ip_user,$id_media){


			$result = ORM::for_table('likes')
						->raw_query('SELECT DISTINCT u.user_id FROM likes l, users u WHERE l.media_id = :mediaID AND u.user_id = l.user_id AND u.user_ip = :userIP AND l.user_id != :userID ', array('userID' => $id_user, 'userIP' => $ip_user, 'mediaID' => $id_media))
						->find_many();
			return $result;
	}

	public static function deleteFakeLikes($id_like){


			$result = ORM::for_table('likes')
    					->where_equal('like_id', $id_like)
    					->delete_many();
	}

	public static function deleteFakeUsers($id_user){

			$result = ORM::for_table('users')->find_one($id_user);
    					$result->user_email = "NULL";

    		// Syncronise the object with the database
    		$result->save();

	}

	public static function updateLikes($id_media,$likes){


			$result = ORM::for_table('medias')->find_one($id_media);
    					$result->media_likes = $likes;

    		// Syncronise the object with the database
    		$result->save();
	}

	/**
	 * Magic method for accessing the elements of the private
	 * $orm instance as properties of the user object
	 * @param string $key The accessed property's name 
	 * @return mixed
	 */

	public function __get($key){
		if(isset($this->orm->$key)){
			return $this->orm->$key;
		}

		return null;
	}

}