<?php 
/**
 * Redirect to link page
 * @param string $url - URl to redirect
 * @return go to the URL
 **/ 

function redirect($url){

	header("Location: $url");
	exit;

}

/**
 * Send e-mail with Mandrillapp
 * @param string $nameFrom - Sender name
 * @param string $mailForm - Sender e-mail
 * @param string $subject - E-mail Subject
 * @param string $message - E-mail Message
 * @param string $mailTo - Receiver e-mail
 * @param string $nameTo - Receiver name
 * @return send e-mail
 **/ 

function sendMail ($nameFrom, $mailFrom, $subject, $message, $mailTo, $nameTo) {

	// Sanitize variables
	$nameFrom = trim(stripslashes($nameFrom));
	$mailFrom = trim(strtolower($mailFrom));
	$subject = trim(stripslashes($subject));
	$message = trim(stripslashes($message));
	$mailTo = trim(strtolower($mailTo));
	$nameTo = trim(strtolower($nameTo));

	// More informations
	$sendDate = date('H:i:s Y-m-d ');  // Format date de MySQL
	$ip = $_SERVER['REMOTE_ADDR'];
	    
	require ('libs/mandrillapp/vendor/autoload.php');

	try {
	    $mandrill = new Mandrill(_MAND_API_KEY_);
	    $message = array(
	        // the full HTML content to be sent
	        'html' => $message,

	        // optional full text content to be sent
	        'text' => strip_tags($message),
	        'subject' => $subject,
	        'from_email' => $mailFrom,
	        'from_name' => $nameFrom,
	        'to' => array(
	            array(
	                'email' => $mailTo,
	                'name' => $nameTo,
	                'type' => 'to'
	            )
	        ),
	        'headers' => array('Reply-To' => $mailFrom),
	        'important' => true,
	        'track_opens' => true,
	        'track_clicks' => null,
	        'auto_text' => null,
	        'auto_html' => true,
	        'inline_css' => null,
	        'url_strip_qs' => null,
	        'preserve_recipients' => null,
	        'view_content_link' => null,
	        'tracking_domain' => null,
	        'signing_domain' => null,
	        'return_path_domain' => null,
	        'tags' => array('contact-mail'),
	        'subaccount' => _MAND_SUB_ACCOUNT_
	    );
	    $async = false;
	    $result = $mandrill->messages->send($message, $async);

	} catch(Mandrill_Error $e) {
	    // Mandrill errors are thrown as exceptions
	    echo 'A mandrill error occurred: ' . get_class($e) . ' - ' . $e->getMessage();
	    // A mandrill error occurred: Mandrill_Unknown_Subaccount - No subaccount exists with the id 'customer-123'
	    throw $e;
	}
}


/**
 * To Logout user
 * @return Kill user session
 **/ 


function logout() {
	$_SESSION = array();
	unset($_SESSION);
}

/**
 * Utilisé pour is_email() - 
 * Sur les machines Windows cette fonction n'est pas disponible donc cette fonction sera utilisée à la place
 * Permet de voir à partir d'une adresse web si l'hebergeur existe
 * @param NONE
 * @return true si valide ou false si non
 **/ 

if (!function_exists("checkdnsrr")) {

  function checkdnsrr() {
    return true;
  }

}

/**
 * Checks si l'addresse e-mail est valide et si l'hebergeur existe
 * @param string $email - Email à vérifier
 * @return true si valide ou false si non
 **/ 

function is_email($email) {

  $email = trim($email);

  if (!$email) return false;

  if (!preg_match("!^[\w|\.|\-|_]+@\w[\w|\.|\-]+\.[a-zA-Z]{2,6}$!",$email)) return false;

  return true;

  // Il faut internet pour que ce bout de code fonctionne, et voilà

  // else {

  //   list($user, $host) = explode("@", $email);
  //   if (checkdnsrr($host, "MX") or checkdnsrr($host, "A")) return true;
  //   else return false;

  // }

}

// Hashage du mot de passe dans la base de données avec la clé SALT
function hashPassword($pw){

	return sha1(_AUTH_SALT_.md5($pw._AUTH_SALT_).sha1(_AUTH_SALT_));

}

// Hash ID
function hashId($id){

	return sha1(md5($id).sha1($id));

}