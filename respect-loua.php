<?php 
include_once ("inc/config.php");


?>
<!DOCTYPE html>
<!--[if lt IE 7]><html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]><html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]> <html class="no-js lt-ie9"><![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js" lang="fr"> <!--<![endif]-->
<!--[if lt IE 9]><script src="//html5shiv.googlecode.com/svn/trunk/html5.js"></script><![endif]-->
<html lang="fr">

	<head>
		<meta charset="utf-8" />
		<title>Elles font respecter la LOUA</title>

		<meta http-equiv="content-type" content="text/html; charset=utf-8" />
		<meta name="description" content="Découvrez les témoignages des utilisatrices de LOUA. Vanessa Lawrens nous parle de Loua." />
		<meta name="viewport" content="initial-scale=1,minimum-scale=1,width=device-width">

		<!-- Open Graph facebook -->
		<meta property="og:url" content="http://"/>
		<meta property="og:title" content=""/> 
		<meta property="og:type" content="website"/> 
		<meta property="og:image" content="http://"/> 
		<meta property="og:site_name" content=""/> 
		<meta property="og:description" content=""/>
		<meta name="viewport" content="initial-scale=1,minimum-scale=1,width=device-width">

		<?php include_once("header.php"); ?>

			<div class="content">
				<div class="cols-row">
					<div class="col-100">
						<img class="rounded" src="<?php echo _INSTDIR_; ?>img/respecter-loua-header.jpg" alt="">	
					</div>
				</div>
				<div class="cols-row respect-content">
					<div class="cols-row begin end">
						<div class="col-66">
							<h1 class="upper">Elles font respecter la LOUA</h1>
						</div>
					</div>
					<div class="cols-row">
						<div class="col-66">
							<div class="cols-row">
								<div class="col-50">
									<img src="<?php echo _INSTDIR_; ?>img/temoin-vanessa.jpg" alt="">
								</div>
								<div class="col-50">
									<h2>Témoignage de <br><strong>Vanessa Lawrens</strong></h2>
									<p>« Avec les 12 bandes de cire froide LOUA, la peau lisse c’est MOUA ! Ultra pratiques dans leur sachet fin, les bandes me suivent partout pour être parfaite à tout instant. Elles éliminent même les poils les plus courts et laissent ma peau douce pendant longtemps !</p>
								</div>
							</div>

						</div>
						<div class="col-33">
							<a href="<?php echo _INSTDIR_; ?>nos-bandes/bande-cire-froide-jambes">
								<img src="<?php echo _INSTDIR_; ?>img/sidebar-brigade.jpg" alt="">
							</a>
						</div>
					</div>

					<div class="cols-row">
						<div class="col-66">
							<div class="cols-row">
								<div class="col-50">
									<img src="<?php echo _INSTDIR_; ?>img/temoin-1.jpg" alt="">
								</div>
								<div class="col-50">
									<h2>Blog<br><strong>Needs and Moods </strong></h2>
									<p>« J’ai trouvé les packagings super jolis et très actuels, avec des bulles façon BD  […] et des petits messages amusants » « Chacun de ces kits d’épilation contient tout le nécessaire pour se faire une peau impeccable […] ce sont donc des kits très complets, parfaits pour l’épilation nomade en vacances ou en weekend.</p>
									<p>Retrouve la totalité du témoignage sur <a href="http://www.needsandmoods.com/epilation-a-la-maison/" target="_blank">http://www.needsandmoods.com/epilation-a-la-maison/</a></p>
								</div>
							</div>

						</div>
						<div class="col-33">
						</div>
					</div>

					<div class="cols-row">
						<div class="col-66">
							<div class="cols-row">
								<div class="col-50">
									<img src="<?php echo _INSTDIR_; ?>img/temoin-2.jpg" alt="">
								</div>
								<div class="col-50">
									<h2>Blog<br><strong>Cherryblossom</strong></h2>
									<p>« Leur packaging est hyper girly. J’adore ! […]  Leur petite taille est impeccable pour parfaire l'épilation en éliminant les éventuels petits poils résistants à l'envahisseur! »</p>
									<p>Retrouve la totalité du témoignage sur <a href="http://cherryblossom.eklablog.com/remportez-1-an-de-produits-pour-l-epilation-avec-laurence-dumont-a115234568" target='_blank'>http://cherryblossom.eklablog.com/remportez-1-an-de-produits-pour-l-epilation-avec-laurence-dumont-a115234568</a></p>
								</div>
							</div>

						</div>
						<div class="col-33">
						</div>
					</div>

					<div class="cols-row">
						<div class="col-66">
							<div class="cols-row">
								<div class="col-50">
									<img src="<?php echo _INSTDIR_; ?>img/temoin-3.jpg" alt="">
								</div>
								<div class="col-50">
									<h2>Blog<br><strong>Le Journal de Crapette </strong></h2>
									<p>« Des petits kits de cires froide aux packagings bien pratiques et girly à emmener partout avec soi. […] Les pochettes de bandes Loua sont particulièrement adaptées pour les retouches vacances. Les petites pochettes sont légères et très fines. […] La cire sent très bon et arrache très bien les poils. »</p>
									<p>Retrouve la totalité du témoignage sur <a href="http://www.lejournaldecrapette.fr/une-epilation-au-poil-avec-laurence-dumont-concours/" traget="_blank">http://www.lejournaldecrapette.fr/une-epilation-au-poil-avec-laurence-dumont-concours/</a></p>
								</div>
							</div>

						</div>
						<div class="col-33">
						</div>
					</div>

					<div class="cols-row">
						<div class="col-66">
							<div class="cols-row">
								<div class="col-50">
									<img src="<?php echo _INSTDIR_; ?>img/temoin-4.jpg" alt="">
								</div>
								<div class="col-50">
									<h2>Blog de<br><strong>Silana</strong></h2>
									<p>« La globetrotteuse que je suis a eu un énorme coup de coeur pour ce produit qui se décline en quatre versions (maillot, aisselles, visage, jambes). […] Il s'agit de paquets de 12 bandes de cire froide au packaging rétro qui font référence aux pin-up des sixties. […] Le format ultra plat est idéal pour le glisser dans une valise et les emmener avec soi pour un week-end à l'étranger par exemple. »</p>
									<p>Retrouve la totalité du témoignage sur <a href="http://silana-blog.com/index.php?post/avis-Laurence-Dumont-%C3%A9pilation-cire-test" target="_blank">http://silana-blog.com/index.php?post/avis-Laurence-Dumont-%C3%A9pilation-cire-test</a></p>
								</div>
							</div>

						</div>
						<div class="col-33">
						</div>
					</div>

				</div>
			
				<?php include_once("footer.php"); ?>
			</div>
			
		</div>
		
		<!-- <script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script> -->
		<script src="js/jquery.min.js"></script>
		<script src="js/main.js"></script>

	</body>
</html>