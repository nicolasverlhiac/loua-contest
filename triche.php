<?php 
require_once ("inc/config.php");
require_once ("inc/class.contest.php");
require_once ("inc/functions.php");

$contest = new Contest();

$id = $_GET["id"];
$kill = $_GET["kill"];

// Information de l'utilisateur
$user = $contest->displayLegfieInfo($id);
$media = $contest->displayMedia($id);

$i = 0;
$b = 0;
$triche = $contest->countFake($id,$user[0]["user_ip"],$media[0]["media_id"]);
$tricheurs = $contest->selectFakeUsers($id,$user[0]["user_ip"],$media[0]["media_id"]);
?>

<!DOCTYPE html>
<!--[if lt IE 7]><html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]><html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]> <html class="no-js lt-ie9"><![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js" lang="fr"> <!--<![endif]-->
<!--[if lt IE 9]><script src="//html5shiv.googlecode.com/svn/trunk/html5.js"></script><![endif]-->
<html lang="fr">

	<head>
		<meta charset="utf-8" />
		<title>Legfies de <?php echo $user[0]["user_login"]; ?> - Loua</title>

		<meta http-equiv="content-type" content="text/html; charset=utf-8" />
		<meta name="description" content="Participe à l’élection des plus beaux Legfies et tente de gagner 1 voyage pour 2 aux Antilles  et plein d’autres cadeaux! " />
		<meta name="viewport" content="initial-scale=1,minimum-scale=1,width=device-width">

		<?php include_once("header.php"); ?>

			<div class="content">

				<table class="width-100">	
							<thead>
								<tr>
									<th class="width-20">Classement</th>
									<th class="width-20">Mail</th>
									<th class="width-30">Inscription</th>
									<th class="width-30">User ID</th>
								</tr>
							</thead>
							<tbody>
								
								<?php foreach ($triche as $tricheur): ?>
									<tr>
										<td><?php $i++; echo $i; ?></td>
										<td><?php echo $tricheur->user_email; ?></td>
										<td><?php echo $tricheur->media_registered; ?></td>
										<td><a href="<?php echo _INSTDIR_."candidats.php?id=".$tricheur->user_id; ?>"><?php echo $tricheur->user_id; ?></a></td>

										<?php 
										if ($kill == 1) {

											$contest->deleteFakeLikes($tricheur->like_id);

										}

										?>
									</tr>
								<?php endforeach; ?>


								<?php
									if ($kill == 1) {

										$realLikes = $media[0]["media_likes"] - $i;
										echo $realLikes;
										$contest->updateLikes($media[0]["media_id"],$realLikes);
									}
								?>

							</tbody>
						</table>

					<table class="width-100">	
							<thead>
								<tr>
									<th class="width-50">Classement</th>
									<th class="width-50">User ID</th>
								</tr>
							</thead>
							<tbody>

						<?php foreach ($tricheurs as $fakeUser): ?>
									<tr>
										<td><?php $b++; echo $b; ?></td>
										<td><a href="<?php echo _INSTDIR_."candidats.php?id=".$fakeUser->user_id; ?>"><?php echo $fakeUser->user_id; ?></a></td>

										<?php 
										if ($kill == 1) {

											$contest->deleteFakeUsers($fakeUser->user_id);

										}

										?>
									</tr>
								<?php endforeach; ?>
							</tbody>
						</table>

						<div class="cols-row">
							<div class="col-100">
								<a href="<?php echo _INSTDIR_."triche.php?kill=1&id=".$id; ?>" class="btn-form width-100 centered"><span class="icon-trash"></span> Clean DataBase</a>
							</div>
						</div>

				<?php include_once("footer.php"); ?>
			</div>
			
		</div>
		
		<!-- <script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script> -->
		<script src="<?php echo _INSTDIR_; ?>js/jquery.min.js"></script>
		<script src="<?php echo _INSTDIR_; ?>js/main.js"></script>

	</body>
</html>