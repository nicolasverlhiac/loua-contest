<h1>Le poil Dru</h1>
<h2>Fiche d’identité</h2>
<p>C’est un poil très combatif, un délinquant pileux multi récidiviste ! Il n’hésite pas à revenir sur le champ de bataille pour maintenir sa position. Plus dur, plus épais, plus noir, le poil dru s’avère être un ennemi de taille dans la guerre des poils. Son point faible ? Il est facilement détectable à la vue et au toucher. La présence de cet indésirable signalée, il est alors nécessaire de procéder à son interpellation rapidement, avant qu’il ne gagne du terrain.</p>

<h2>Motif d’arrestation</h2>
<p>Flagrant délit de repousse et atteinte à la douceur. Ses caractéristiques le rendent extrêmement disgracieux et tenace.  Un indésirable que nous ne pouvons laisser pousser en liberté !</p>
<p><a class="btn-form" href="http://loua.fr/videos">DÉCOUVRE L’INTERROGATOIRE DE LA PEAU LISSE </a></p>