<h1>Le poil Incarné</h1>
<h2>Fiche d’identité</h2>
<p>Au milieu des poils naissants qui pointent le bout de leur nez se trouve un ennemi encore plus indésirable…le poil incarné. Membre imminent dans la hiérarchie du crime organisé, il se cache afin de mieux servir sous la peau. Il pousse sans jamais se découvrir à la surface. Malgré l’ingéniosité de son stratagème, il est facile de le débusquer. Un petit point rouge et gonflé sur la surface de votre peau après l'épilation…vous avez un poil incarné. Les endroits les plus touchés par ce gangster poilu ? Les jambes et le maillot.</p>

<h2>Motif d’arrestation</h2>
<p>Ce délinquant pileux pris en flagrant délit d’outrage à la beauté peut causer de nombreuses dégradations sur votre peau : rougeurs, boutons, inflammations, possibilité d'infection, cicatrices,… Un malfrat récidiviste gênant et disgracieux. </p>

<p><a class="btn-form" href="http://loua.fr/videos">DÉCOUVRE L’INTERROGATOIRE DE LA PEAU LISSE </a></p>