<h1>Le poil Rebelle</h1>
<h2>Fiche d’identité</h2>
<p>Le poil rebelle agit selon une stratégie digne des délinquants les plus pervers…se cacher partout où il sera compliqué de le débusquer et maintenir coûte que coûte sa position. Adepte des zones délicates et difficilement praticables, il connait le terrain par cœur et le maîtrise à la perfection. Cet ennemi récalcitrant nous oblige à nous contorsionner pour intervenir et l’interpeller dans des zones à risques, non sans souffrance. Rendant difficilement les armes, il tentera tout pour éviter l’arrestation fatidique.</p>

<h2>Motif d’arrestation</h2>
<p>L’occupation intempestive de zones interdites avec délit de pilosité préméditée. Vous pensiez être tranquille pour l’été ? Cet individu indésirable est bien souvent la cause d’interventions délicates effectuées en urgence. Un délinquant poilu qu’il faut traquer avec détermination jusque dans les moindres recoins.</p>
<p><a class="btn-form" href="http://loua.fr/videos">DÉCOUVRE L’INTERROGATOIRE DE LA PEAU LISSE </a></p>