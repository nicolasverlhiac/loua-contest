<h1>Le poil Frisé</h1>
<h2>Fiche d’identité</h2>
<p>Ce poil a élaboré une technique de dissimulation plutôt ingénieuse en misant sur son apparence. Poussant en forme de tire-bouchon, il peut aisément s’incruster et pousser sous la peau. Son objectif ? Rejoindre son mentor et allié du crime organisé, le poil incarné ! Il est néanmoins facile de le débusquer grâce à un signalement très précis : sa forme frisée. Ce délinquant poilu ne recule devant aucun artifice pour arriver à ses fins machiavéliques.</p>

<h2>Motif d’arrestation</h2>
<p>Ce poil est accusé de tentative de fuite avec crime sur la beauté prémédité. Une fois l’auteur du délit poilu identifié, il faudra procéder à une interpellation rapide et efficace afin d’éviter tout risque d’évolution vers le poil incarné.</p>
<p><a class="btn-form" href="http://loua.fr/videos">DÉCOUVRE L’INTERROGATOIRE DE LA PEAU LISSE </a></p>