<?php 
require_once ("inc/config.php");
require_once ("inc/class.contest.php");
require_once ("inc/functions.php");

$contest = new Contest();

// Get all legfies
$leggfies = $contest->displayWait();


$countMedia = $contest->countStatsLegfie();

?>


<!DOCTYPE html>
<!--[if lt IE 7]><html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]><html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]> <html class="no-js lt-ie9"><![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js" lang="fr"> <!--<![endif]-->
<!--[if lt IE 9]><script src="//html5shiv.googlecode.com/svn/trunk/html5.js"></script><![endif]-->
<html lang="fr">

	<head>
		<meta charset="utf-8" />
		<title> Administration LOUA</title>

		<meta http-equiv="content-type" content="text/html; charset=utf-8" />
		<meta name="viewport" content="initial-scale=1,minimum-scale=1,width=device-width">

		<!-- Open Graph facebook -->
		<meta property="og:url" content="http://"/>
		<meta property="og:title" content=""/> 
		<meta property="og:type" content="website"/> 
		<meta property="og:image" content="http://"/> 
		<meta property="og:site_name" content=""/> 
		<meta property="og:description" content=""/>
		<meta name="viewport" content="initial-scale=1,minimum-scale=1,width=device-width">

		<?php include_once("header.php"); ?>

			<div class="content">
				<h2>Validation</h2>
				<div class="cols-row">
					<div class="col-100">
						<ul class="galery legfie-img">
						<?php foreach ($leggfies as $legfie): ?>
							<li>
								<a href="<?php echo _INSTDIR_."legfies/".$legfie->media_id ?>">
									<img src="<?php echo _INSTDIR_.$legfie->media_url ?>" alt="">		
								</a>
								<a href="<?php echo _INSTDIR_; ?>admin-validate.php?id=<?php echo $legfie->media_id; ?>&key=1&iduser=<?php echo $legfie->user_id; ?>&url=http://loua.fr/legfies/<?php echo $legfie->media_id; ?>" class="btn-form width-50 left">Valider</a> <a href="<?php echo _INSTDIR_; ?>admin-validate.php?id=<?php echo $legfie->media_id; ?>&iduser=<?php echo $legfie->user_id; ?>&key=2" class="btn-form right">X</a>
							</li>
						<?php endforeach; ?>
						</ul>		
					</div>
				</div>
				
				<h2>Statistiques</h2>
				<div class="cols-row">
					<div class="col-100">
						<table class="width-100 table-bords">	
							<thead>
								<tr>
									<th class="width-50">Date</th>
									<th class="width-50">Legfies</th>
								</tr>
							</thead>
							<tbody>
								<?php 
									for ($i=2; $i <= date("j"); $i++) { 

										echo "<tr><td>".$i." Juillet 2015 </td>";
										echo "<td>";
										$count = 0;
										foreach ($countMedia as $media){
											if (date('d', strtotime($media->media_registered)) == $i){
													$count++;
											}
										}
										echo $count;
										echo "</td></tr>";
									}
								 ?>
							</tbody>
							<tfoot>
								<tr>
									<td><strong>TOTAL</strong></td>
									<td>
										<strong>
										<?php // Total
											$countTotal = 0;
											foreach ($countMedia as $media){ $countTotal++; }
											echo $countTotal;
										?>
										</strong>
									</td>
								</tr>
							</tfoot>
						</table>
					</div>
				</div>


				<?php include_once("footer.php"); ?>
			</div>
			
		</div>
		
		<!-- <script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script> -->
		<script src="<?php echo _INSTDIR_; ?>js/jquery.min.js"></script>
		<script src="<?php echo _INSTDIR_; ?>js/main.js"></script>

	</body>
</html>