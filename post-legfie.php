<?php 
include_once ("inc/config.php");


?>
<!DOCTYPE html>
<!--[if lt IE 7]><html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]><html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]> <html class="no-js lt-ie9"><![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js" lang="fr"> <!--<![endif]-->
<!--[if lt IE 9]><script src="//html5shiv.googlecode.com/svn/trunk/html5.js"></script><![endif]-->
<html lang="fr">

	<head>
		<meta charset="utf-8" />
		<title>Postez un Legfie</title>

		<meta http-equiv="content-type" content="text/html; charset=utf-8" />
		<meta name="description" content="Pour participer au jeux concours, il faut envoyer sont plus beau legfie." />
		<meta name="viewport" content="initial-scale=1,minimum-scale=1,width=device-width">

		<!-- Open Graph facebook -->
		<meta property="og:url" content="http://"/>
		<meta property="og:title" content=""/> 
		<meta property="og:type" content="website"/> 
		<meta property="og:image" content="http://"/> 
		<meta property="og:site_name" content=""/> 
		<meta property="og:description" content=""/>
		<meta name="viewport" content="initial-scale=1,minimum-scale=1,width=device-width">
			
		<link href="<?php echo _INSTDIR_; ?>crop/css/bootstrap.min.css" rel="stylesheet">
		<link href="<?php echo _INSTDIR_; ?>crop/css/cropper.min.css" rel="stylesheet">
		<link href="<?php echo _INSTDIR_; ?>crop/css/main.css" rel="stylesheet">

		<!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
		<!--[if lt IE 9]>
		  <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
		  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
		<![endif]-->

		<?php include_once("header.php"); ?>


			<div class="content">

				<div class="cols-row end begin">
					<div class="col-80 col-centre">
						<div class="cols-row centered menu-game">
							
							<div class="col-66 col-centre">
								<a href="<?php echo _INSTDIR_; ?>legfies" class="bleu">
									Je visite la galerie des legfies
									<br> & <br>Je vais voter !
								</a>
							</div>
						</div>
					</div>
				</div>
				
				<div class="cols-row">
					<div class="col-100 add-legfie centered">
						<strong>ou</strong>
						<br>
						<h2>Envoie ton Legfie</h2>
					</div>
				</div>
				
				<?php if (isset($_SESSION['loginId'])){ ?>
				<div class="cols-row">

					<div class="col-50">

						<div class="" id="crop-avatar">

						    <!-- Current avatar -->
						    <div class="avatar-view" title="Choisissez votre legfie">
						    <?php if (!isset($_SESSION['login'])) { ?>
						    	<img src="<?php echo _INSTDIR_; ?>img/legfie.jpg" alt="Avatar">
						    <?php } else { ?>
								<img src="<?php echo _INSTDIR_; ?>img/legfie.jpg" alt="Avatar">
						    <?php }?>
						    </div>

						    <!-- Cropping modal -->
						    <div class="modal fade" id="avatar-modal" aria-hidden="true" aria-labelledby="avatar-modal-label" role="dialog" tabindex="-1">
						      <div class="modal-dialog modal-lg">
						        <div class="modal-content">
						          <form class="avatar-form" action="<?php echo _INSTDIR_; ?>class.crop.php" enctype="multipart/form-data" method="post">
						            <div class="modal-header">
						              <button class="close" data-dismiss="modal" type="button">&times;</button>
						              <h4 class="modal-title" id="avatar-modal-label">Envoyez votre legfie</h4>
						            </div>
						            <div class="modal-body">
						              <div class="avatar-body">

						                <!-- Upload image and data -->
						                <div class="avatar-upload">
						                  <input class="avatar-src" name="avatar_src" type="hidden">
						                  <input class="avatar-data" name="avatar_data" type="hidden">
						                  <label for="avatarInput">Uploadez votre image</label>
						                  <input class="avatar-input" id="avatarInput" name="avatar_file" type="file">
						                </div>

						                <!-- Crop and preview -->
						                <div class="row">
						                  <div class="col-md-9">
						                    <div class="avatar-wrapper"></div>
						                  </div>
						                  <div class="col-md-3">
						                    <div class="avatar-preview preview-lg"></div>
						                    <div class="avatar-preview preview-md"></div>
						                    <div class="avatar-preview preview-sm"></div>
						                  </div>
						                </div>

						                <div class="row avatar-btns">
						                  <div class="col-md-9">
						                    <div class="btn-group">
						                      <button class="btn btn-primary" data-method="rotate" data-option="-90" type="button" title="Rotate -90 degrees">Rotation Gauche</button>
						                      <button class="btn btn-primary" data-method="rotate" data-option="-15" type="button">-15deg</button>
						                      <button class="btn btn-primary" data-method="rotate" data-option="-30" type="button">-30deg</button>
						                      <button class="btn btn-primary" data-method="rotate" data-option="-45" type="button">-45deg</button>
						                    </div>
						                    <div class="btn-group">
						                      <button class="btn btn-primary" data-method="rotate" data-option="90" type="button" title="Rotate 90 degrees">Rotation Droite</button>
						                      <button class="btn btn-primary" data-method="rotate" data-option="15" type="button">15deg</button>
						                      <button class="btn btn-primary" data-method="rotate" data-option="30" type="button">30deg</button>
						                      <button class="btn btn-primary" data-method="rotate" data-option="45" type="button">45deg</button>
						                    </div>
						                  </div>
						                  <div class="col-md-3">
						                    <button class="btn btn-primary btn-block avatar-save" type="submit">Envoyer</button>
						                  </div>
						                </div>
						              </div>
						            </div>
						            <!-- <div class="modal-footer">
						              <button class="btn btn-default" data-dismiss="modal" type="button">Close</button>
						            </div> -->
						          </form>
						        </div>
						      </div>
						    </div><!-- /.modal -->

						    <!-- Loading state -->
						    <div class="loading" aria-label="Loading" role="img" tabindex="-1"></div>
						  </div>

					</div>

					<div class="col-50">

						<form id="addlegfie" action="" method="POST">

							<input id="nom" name="candidat_nom" class="input width-100" placeholder="Nom" type="text">

							<input id="prenom" name="candidat_prenom" class="input width-100" placeholder="Prenom" type="text">

							<input id="postal" name="candidat_codepostal" class="input width-100" placeholder="Code postal" type="number">

							<input id="birth" name="candidat_birthday" class="input width-100" placeholder="Année de naissance (ex: 1991 )" type="number">

							<input type="submit" class="btn-form width-100" value="Envoie ton Legfie">

							<div id="report-erreur"></div>

						</form>
					</div>

				</div>
				<?php } else { ?>
					<div class="cols-row">
						<div class="cols-100 centered">
							<p>Pour participer il faut</p>
							<a href="<?php echo _INSTDIR_; ?>jeux" class="btn-form">S'inscrire</a> ou <a href="<?php echo _INSTDIR_; ?>login" class="btn-form">Se connecter</a>
							<p></p>
						</div>
					</div>
				<?php } ?>

				<?php include_once("footer.php"); ?>
			</div>
			
		</div>


		<script src="<?php echo _INSTDIR_; ?>crop/js/jquery.min.js"></script>
		<script src="<?php echo _INSTDIR_; ?>crop/js/bootstrap.min.js"></script>
		<script src="<?php echo _INSTDIR_; ?>crop/js/cropper.min.js"></script>
		<script src="<?php echo _INSTDIR_; ?>crop/js/main.js"></script>
		
		<script src="<?php echo _INSTDIR_; ?>js/main.js"></script>

	</body>
</html>