<?php 
include_once ("inc/config.php");


?>
<!DOCTYPE html>
<!--[if lt IE 7]><html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]><html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]> <html class="no-js lt-ie9"><![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js" lang="fr"> <!--<![endif]-->
<!--[if lt IE 9]><script src="//html5shiv.googlecode.com/svn/trunk/html5.js"></script><![endif]-->
<html lang="fr">

	<head>
		<meta charset="utf-8" />
		<title>Login</title>

		<meta http-equiv="content-type" content="text/html; charset=utf-8" />
		<meta name="description" content="Page de connexion pour participer au jeux concours ou voter pour les legfies des participants." />
		<meta name="viewport" content="initial-scale=1,minimum-scale=1,width=device-width">

		<!-- Open Graph facebook -->
		<meta property="og:url" content="http://"/>
		<meta property="og:title" content=""/> 
		<meta property="og:type" content="website"/> 
		<meta property="og:image" content="http://"/> 
		<meta property="og:site_name" content=""/> 
		<meta property="og:description" content=""/>
		<meta name="viewport" content="initial-scale=1,minimum-scale=1,width=device-width">

		<?php include_once("header.php"); ?>

			<div class="content">
				
				<?php include("g-header.php"); ?>


				<div id="report-erreur"></div>

				<?php if (!isset($_SESSION['loginId'])){ ?>
				<div class="cols-row login-content">
					<div class="col-50">

						<h2>Connectez-vous</h2>
						<form id="login" action="" method="POST">

							<input id="mail1" class="input width-100" placeholder="Adresse e-mail" type="email" name="candidat_mail">

							<input id="psw1" name="candidat_password" class="input width-100" placeholder="Mot de passe" type="password">

							<input type="submit" class="btn-form width-100" value="Connexion">

						</form>
					</div>
					<div class="col-50">
						<h2>Pas de compte ? </h2>
						<a href="<?php echo _INSTDIR_; ?>jeux" class="btn-form width-100">S'inscrire</a>
					</div>
				</div>
				<?php } ?>

				<?php include_once("footer.php"); ?>
			</div>
			
		</div>
		
		<!-- <script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script> -->
		<script src="<?php echo _INSTDIR_; ?>js/jquery.min.js"></script>
		<script src="<?php echo _INSTDIR_; ?>js/main.js"></script>

	</body>
</html>