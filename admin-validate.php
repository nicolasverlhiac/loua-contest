<?php 
require_once ("inc/config.php");
require_once ("inc/class.contest.php");
require_once ("inc/functions.php");

$contest = new Contest();

$key = $_GET["key"]; 
$id = $_GET["id"];

if (isset($_GET["url"])) {
	$url = $_GET["url"];	
}

$iduser = $_GET["iduser"];

// Information de l'utilisateur
$user = $contest->displayLegfieInfo($iduser);

if ($key == 1) {
	$contest->validateLegfieWait($id);

	sendMail (
		'Loua - Concours de Legfies', 
		'a.casemajor@laurence-dumont.fr', 
		'Validation de votre Legfie !', 
		'<p>Bonjour '.$user[0]["user_prenom"].',</p><p>Bravo, ton legfie vient d’être validé ! Tu fais désormais parti de notre brigade de Peau Lisse : <br><a href="'.$url.'">'.$url.'</a></p><p>N’oublies pas de venir voter tous les jours et de partager ta photo pour avoir un maximum de chances de gagner ! <br>Rendez-vous le 7 septembre pour le grand tirage au sort du voyage aux Antilles..</p><p>En attendant, rejoins-nous sur Facebook et tente de gagner des cadeaux chaque semaine !<br> <a href="https://www.facebook.com/LaurenceDumontCosmetiques">Jouez sur Facebook</a> </p><p>A bientôt, <br>La brigade LOUA</p>',
		$user[0]["user_email"],
		'La Brigade Loua'
	);
}

if ($key == 2) {

	$contest->deleteLegfieWait($id);

	sendMail (
		'Loua - Concours de Legfies', 
		'a.casemajor@laurence-dumont.fr', 
		'Candidature refusée', 
		'<p>Bonjour '.$user[0]["user_prenom"].',</p><p>Nous sommes désolé mais ton legfie ne correspond pas aux règles! Tu peux re-tenter ta chance si tu le désire.</p><p>A bientôt, <br>La brigade LOUA</p>',
		$user[0]["user_email"],
		'La Brigade Loua'
	);
	
}

redirect("admin-loua-LD2015");
?>