<?php 
include_once ("inc/config.php");

sendMail (
	'Loua - Debug', 
	_MAIL_ADMIN_, 
	'Erreur 404', 
	'Bonjour Nicolas, une 404 vient de remonter par '.$_SERVER['REMOTE_ADDR'].'. <br> <strong> URL : '.$_SERVER['REQUEST_URI'].'</strong> . <br><br>Rapport du '.strftime("%A").date(' d-m-Y \à H:i:s').' .<br> Odin', 
	_MAIL_WEBMASTER_,
	'Nicolas LAREVICH'
	);
 ?>
<!DOCTYPE html>
<!--[if lt IE 7]><html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]><html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]> <html class="no-js lt-ie9"><![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js" lang="fr"> <!--<![endif]-->
<!--[if lt IE 9]><script src="//html5shiv.googlecode.com/svn/trunk/html5.js"></script><![endif]-->
<html lang="fr">

	<head>
		<meta charset="utf-8" />
		<title>Loua - 404</title>

		<meta http-equiv="content-type" content="text/html; charset=utf-8" />
		<meta name="description" content="Page 404 pour les ceux qui ce sont perdu .." />
		<meta name="viewport" content="initial-scale=1,minimum-scale=1,width=device-width">

		<!-- Open Graph facebook -->
		<meta property="og:url" content="http://"/>
		<meta property="og:title" content=""/> 
		<meta property="og:type" content="website"/> 
		<meta property="og:image" content="http://"/> 
		<meta property="og:site_name" content=""/> 
		<meta property="og:description" content=""/>
		<meta name="viewport" content="initial-scale=1,minimum-scale=1,width=device-width">

		<?php include_once("header.php"); ?>

			<div class="content centered">

				<div class="cols-row">
					<div class="col-100">
						<h1>404</h1>
						<h1 class="soustitre">Vous ne trouverez aucun poil sur cette page</h1>	
					</div>
				</div>
				<div class="cols-row">
					<div class="col-100">
						<a href="http://loua.fr" class="btn-form">Retour Accueil</a>
					</div>
				</div>

				<?php include_once("footer.php"); ?>
			</div>
			
		</div>

	</body>
</html>