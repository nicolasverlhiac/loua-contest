<?php 
require_once ("inc/config.php");
require_once ("inc/class.contest.php");
require_once ("inc/functions.php");

$contest = new Contest();

// Get all legfies
$leggfies = $contest->displayWinner();
$i = 1; 


?>


<!DOCTYPE html>
<!--[if lt IE 7]><html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]><html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]> <html class="no-js lt-ie9"><![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js" lang="fr"> <!--<![endif]-->
<!--[if lt IE 9]><script src="//html5shiv.googlecode.com/svn/trunk/html5.js"></script><![endif]-->
<html lang="fr">

	<head>
		<meta charset="utf-8" />
		<title>Winner - Administration LOUA</title>

		<meta http-equiv="content-type" content="text/html; charset=utf-8" />
		<meta name="viewport" content="initial-scale=1,minimum-scale=1,width=device-width">
		<meta name="viewport" content="initial-scale=1,minimum-scale=1,width=device-width">

		<?php include_once("header.php"); ?>

			<div class="content">
				<h2>Winner</h2>
				<div class="cols-row">
					<div class="col-100">

						<table class="width-100">	
							<thead>
								<tr>
									<th class="width-20">Classement</th>
									<th class="width-20">Likes</th>
									<th class="width-30">Inscription</th>
									<th class="width-20">User ID</th>
									<th class="width-10">Triche</th>
								</tr>
							</thead>
							<tbody>

								<?php foreach ($leggfies as $legfie): ?>
									<tr>
										<td><?php echo $i; $i++ ?></td>
										<td><?php echo $legfie->media_likes; ?></td>
										<td><?php echo $legfie->media_registered; ?></td>
										<td><a href="<?php echo _INSTDIR_."candidats.php?id=".$legfie->user_id; ?>"><span class="icon-user"></span>	<?php echo $legfie->user_id; ?></a></td>
										<td><a href="<?php echo _INSTDIR_."triche.php?kill=0&id=".$legfie->user_id; ?>"><span class="icon-target"></span></a></td>
									</tr>
								<?php endforeach; ?>
							</tbody>
						</table>
						
					</div>
				</div>

				<?php include_once("footer.php"); ?>
			</div>
			
		</div>
		
		<!-- <script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script> -->
		<script src="<?php echo _INSTDIR_; ?>js/jquery.min.js"></script>
		<script src="<?php echo _INSTDIR_; ?>js/main.js"></script>

	</body>
</html>