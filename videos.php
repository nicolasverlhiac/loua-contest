<?php 
include_once ("inc/config.php");


?>
<!DOCTYPE html>
<!--[if lt IE 7]><html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]><html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]> <html class="no-js lt-ie9"><![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js" lang="fr"> <!--<![endif]-->
<!--[if lt IE 9]><script src="//html5shiv.googlecode.com/svn/trunk/html5.js"></script><![endif]-->
<html lang="fr">

	<head>
		<meta charset="utf-8" />
		<title>Videoripilante Loua</title>

		<meta http-equiv="content-type" content="text/html; charset=utf-8" />
		<meta name="description" content="LOUA, c’est la nouvelle Brigade Anti-Poils qui lutte contre les délinquants pileux multi-récidivistes !Avec l'arrivée de ces bandes de cire froide, nos ennemis les poils ne peuvent plus échapper à la Peau Lisse!" />
		<meta name="viewport" content="initial-scale=1,minimum-scale=1,width=device-width">

		<!-- Open Graph facebook -->
		<meta property="og:url" content="http://"/>
		<meta property="og:title" content=""/> 
		<meta property="og:type" content="website"/> 
		<meta property="og:image" content="http://"/> 
		<meta property="og:site_name" content=""/> 
		<meta property="og:description" content=""/>
		<meta name="viewport" content="initial-scale=1,minimum-scale=1,width=device-width">

		<?php include_once("header.php"); ?>

			<div class="content">
				<div class="cols-row">
					<div class="col-100">
						<img src="<?php echo _INSTDIR_; ?>img/video-titre.jpg" alt="">
					</div>
				</div>
				<div class="cols-row">
					<div class="col-50">
						<div class="video-responsive">
							<iframe width="560" height="315" src="https://www.youtube.com/embed/soJ8O1cZHSM" frameborder="0" allowfullscreen></iframe>
						</div>
					</div>
					<div class="col-50">
						<div class="video-responsive">
							<iframe width="560" height="315" src="https://www.youtube.com/embed/o7jw8alAVsQ" frameborder="0" allowfullscreen></iframe>
						</div>
					</div>
				</div>
				<div class="cols-row">
					<div class="col-50">
						<div class="video-responsive">
							<iframe width="560" height="315" src="https://www.youtube.com/embed/tBEgMdQoen8" frameborder="0" allowfullscreen></iframe>
						</div>
					</div>
					<div class="col-50">
						<div class="video-responsive">
							<iframe width="560" height="315" src="https://www.youtube.com/embed/9jI2OssDqv8" frameborder="0" allowfullscreen></iframe>
						</div>
					</div>
				</div>
				<?php include_once("footer.php"); ?>
			</div>
			
		</div>
		
		<!-- <script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script> -->
		<script src="<?php echo _INSTDIR_; ?>js/jquery.min.js"></script>
		<script src="<?php echo _INSTDIR_; ?>js/main.js"></script>

	</body>
</html>