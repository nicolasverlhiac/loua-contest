<?php 
include_once ("inc/config.php");


?>
<!DOCTYPE html>
<!--[if lt IE 7]><html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]><html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]> <html class="no-js lt-ie9"><![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js" lang="fr"> <!--<![endif]-->
<!--[if lt IE 9]><script src="//html5shiv.googlecode.com/svn/trunk/html5.js"></script><![endif]-->
<html lang="fr">

	<head>
		<meta charset="utf-8" />
		<title>Mentions Légales</title>

		<meta http-equiv="content-type" content="text/html; charset=utf-8" />
		<meta name="description" content="Mentions Légales du site LOUA" />
		<meta name="viewport" content="initial-scale=1,minimum-scale=1,width=device-width">

		<!-- Open Graph facebook -->
		<meta property="og:url" content="http://"/>
		<meta property="og:title" content=""/> 
		<meta property="og:type" content="website"/> 
		<meta property="og:image" content="http://"/> 
		<meta property="og:site_name" content=""/> 
		<meta property="og:description" content=""/>
		<meta name="viewport" content="initial-scale=1,minimum-scale=1,width=device-width">

		<?php include_once("header.php"); ?>

			<div class="content">
				<div class="cols-row respect-content">
					<div class="cols-row begin end">
						<div class="col-66 col-centre centered">
							<h1 class="upper">Mentions Très Légales</h1>
						</div>
					</div>
					<div class="cols-row">
						<div class="col-100">
							<h2>Mentions légales</h2>
							<p>Conformément à la loi Informatique et Libertés du 6 Janvier 1978, vous disposez d’un droit d'accès, de rectification ou de suppression de vos données personnelles. Vous pouvez exercer ce droit en utilisant la page contact de ce site ou en nous écrivant à l'adresse mentionnée sur cette page. L’ensemble du Site et chacun de ses éléments pris séparément relèvent de la législation française et internationale sur la propriété intellectuelle (droits d’auteur, marques, bases de données …). A ce titre, tous les droits de reproduction, de représentation et de communication publique sont réservés y compris pour les documents téléchargeables et les représentations visuelles, iconographiques, photographiques, audiovisuelles ou autres. Aucune licence, ni aucun autre droit que celui de consulter le Site n'est conféré à quiconque au regard des droits de propriété intellectuelle. La reproduction de tout ou partie du Site sur un support électronique quel qu’il soit est formellement interdite sauf autorisation expresse du chef de publication. Les informations contenues sur le site ne sont pas contractuelles. Elles peuvent être modifiées sans préavis.</p>
						</div>
					</div>
					<div class="cols-row">
						<div class="col-100">
							<h2>Propriétaire du site</h2>
							<p>Laurence Dumont-Allée de Saylat-Zac Agropole-CS20044-47901 Agen Cedex 9</p>
							<p>Téléphone : 05 53 95 92 19 Numéro d’immatriculation au registre du commerce et des sociétés</p>
							<p>SIRET: 39806306500047 RCS: Agen SAS au capital de 160208 euros N°CNIL: 1851132V0</p>
							<p>Président : Régis LELONG</p>

						</div>
					</div>
					<div class="cols-row">
						<div class="col-100">
							<h2>Hébergement</h2>
							<p>Laurence Dumont-Allée de Saylat-Zac Agropole-CS20044-47901 Agen Cedex 9</p>
							<p>Téléphone : 05 53 95 92 19 Numéro d’immatriculation au registre du commerce et des sociétés</p>
							<p>SIRET: 39806306500047 RCS: Agen SAS au capital de 160208 euros N°CNIL: 1851132V0</p>
							<p>Président : Régis LELONG</p>
						</div>
					</div>
					<div class="cols-row">
						<div class="col-100">
							<h2>Création, Design et Webmastering</h2>
							<p>SMALL CREATIVE UNIT – Société La Caixa de la Llum -  Avgda.Meritxell, 75-Ed. Quars, 3 - AD500 Andorra la vella - Principat d'Andorra <br> Contact: contact@smallcreativeunit.com ou caixadelallum@gmail.com </p>
						</div>
					</div>

				</div>
			
				<?php include_once("footer.php"); ?>
			</div>
			
		</div>
		
		<!-- <script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script> -->
		<script src="js/jquery.min.js"></script>
		<script src="js/main.js"></script>

	</body>
</html>