<div class="cols-row end">
	<div class="col-70">
		
		<?php if ( _URL_ == "/login" || _URL_ == "/post-legfie" || _URL_ == "/jeux") { ?>
		<a href="https://www.facebook.com/LaurenceDumontCosmetiques?app_data=%7B%7D&v=app_160812950681114" target="_blank">
			<img class="rounded" src="<?php echo _INSTDIR_; ?>img/fb-footer.jpg" alt="">
		</a>
		<?php } else { ?>

		<div class="cols-row end begin">
			<a href="<?php echo _INSTDIR_; ?>elles-font-respecter-la-loua">
				<div class="col-50 centered temoins">
					<h4>Elles font<br>respecter <br> La Loua</h4>
				</div>
			</a>
			<div class="col-50 no-mobile">
				<a href="<?php echo _INSTDIR_; ?>elles-font-respecter-la-loua">
					<div class="cols-row end begin bloc-temoin rounded">
						<div class="col-33">
							<img src="<?php echo _INSTDIR_; ?>img/footer-bloc.jpg" alt="">
						</div>
						<div class="col-66">
							<p>
								" Avec les 12 bandes de cire froide LOUA, la peau lisse c’est MOUA ! Ultra pratiques dans leur sachet fin, les bandes me ... " 
							</p>
						</div>
					</div>
				</a>
			</div>
		</div>
		<?php } ?>
	</div>
	<div class="col-30">
		<div class="social-titre">
			<img src="<?php echo _INSTDIR_; ?>img/social-titre.png" alt="">
		</div>
		<div class="cols-row">
			<div class="col-25 social-25">
				<a href="https://www.facebook.com/LaurenceDumontCosmetiques" target="_blank"> <img src="<?php echo _INSTDIR_; ?>img/facebook.png" alt="" target="_blank"></a>
			</div>
			<!-- <div class="col-25 social-25">
				<a href=""> <img src="<?php echo _INSTDIR_; ?>img/twitter.png" alt=""></a>
			</div> -->
			<div class="col-25 social-25">
				<a href="https://www.youtube.com/channel/UCCBOFJMn0d878qAbFh00Nzg"> <img src="<?php echo _INSTDIR_; ?>img/youtube.png" alt="" target="_blank"></a>
			</div>
			<!-- <div class="col-25 social-25">
				<a href=""> <img src="<?php echo _INSTDIR_; ?>img/instragram.png" alt=""></a>
			</div> -->
		</div>
	</div>
</div>

<div class="cols-row centered footer">
	<div class="col-33">
		<a href="<?php echo _INSTDIR_; ?>hotel-peaulisse">Hotel de Peau Lisse</a>
	</div>
	<div class="col-33">
		<a href="<?php echo _INSTDIR_; ?>reglement-jeux-concours">Respect  de la LOUA</a>
	</div>
	<div class="col-33">
		<a href="<?php echo _INSTDIR_; ?>mentions-legales">Mentions très légales</a>
	</div>
</div>

<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-64704213-1', 'auto');
  ga('send', 'pageview');

</script>