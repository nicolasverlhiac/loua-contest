<div class="cols-row">
	<div class="col-60 centered">
		<img src="<?php echo _INSTDIR_; ?>img/ld-content-dermo.jpg" alt="">
	</div>

	<div class="col-40">
		<h1>Les Dermo Soins</h1>

		<p>Ces 3 soins associés à l’épilation se faufilent partout avec leur petit flacon.  Ils traquent les résidus de cire, les poils incarnés ou la repousse des poils récidivistes. Avec eux les poils n’ont qu’à bien se tenir !</p>
		
		<a href="http://laurence-dumont.fr/index.php?page=epilation&categorie=A&scategorie=8" target="_blank">
			<div class="cols-row ld-link">
				<div class="col-20">
					<img src="<?php echo _INSTDIR_; ?>img/ld-link.png" alt="">
				</div>
				<div class="col-80">
					Découvre plus en détail le trio des dermo soins ici
				</div>
			</div>
		</a>

	</div>
</div>