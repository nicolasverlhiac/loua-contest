<div class="cols-row">
	<div class="col-60 centered">
		<img src="<?php echo _INSTDIR_; ?>img/ld-content-tolerance.jpg" alt="">
	</div>

	<div class="col-40">
		<h1>Laurence Dumont Haute Tolerance</h1>

		<p>Spécialement développées pour les peaux les plus sensibles, les formules Laurence Dumont Haute Tolérance contiennent 70% d’ingrédients d’origine naturelle et sont formulées sans paraben, sans colorants, sans allergènes pour un meilleur respect de la peau.</p>
	</div>
</div>

<div class="cols-row">
	<div class="col-60">
		<p>Les bandes de cire hypoallergéniques ont pour mission de faire disparaître les poils en douceur, pour le plus grand plaisir des peaux sensibles. Sans allergènes, elles limitent les risques de rougeurs et les irritations pour laisser la peau douce et protégée.</p>
		<p>Les crèmes dépilatoires et leurs embouts techniques avec applicateur permettront une utilisation ultra précise et facile. Leur formule est enrichie en huile de rose musquée, aux vertus réparatrices et régénératrices.</p>
	</div>
	<div class="col-40">
		<br>
			<a href="http://laurence-dumont.fr/index.php?page=epilation&categorie=A&gamme=Haute%20Tol%C3%A9rance" target="_blank">
				<div class="cols-row ld-link">
					<div class="col-20">
						<img src="<?php echo _INSTDIR_; ?>img/ld-link.png" alt="">
					</div>
					<div class="col-80">
						Découvre l’unité spéciale douceur Laurence Dumont Haute Tolérance
					</div>
				</div>
			</a>
	</div>
</div>