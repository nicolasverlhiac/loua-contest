<div class="cols-row">
	<div class="col-60 centered">
		<img src="<?php echo _INSTDIR_; ?>img/ld-content-institut.jpg" alt="">
	</div>

	<div class="col-40">
		<h1>Laurence Dumont Institut</h1>

		<p>Ces produits de qualité Institut, à utiliser chez soi, sont reconnus pour leur haute performance avec des résultats professionnels, rapidement et simplement.</p>
		<p>Grande star de l’année, l’EPIL MUG est le coéquipier idéal pour une éradication sans faille des poils les plus courts tout en garantissant une sécurité maximale. </p>
		<p>Plus discrète, l’EPIL DOUCE se glisse partout et peut être dégainée à tout instant pour des retouches entre 2 épilations : plus d’excuses pour ne pas être parfaite avant un RDV surprise!</p>

		
		<a href="http://laurence-dumont.fr/index.php?page=epilation&categorie=A&gamme=Institut" target="_blank">
			<div class="cols-row ld-link">
				<div class="col-20">
					<img src="<?php echo _INSTDIR_; ?>img/ld-link.png" alt="">
				</div>
				<div class="col-80">
					Découvre toute l’unité spéciale Laurence Dumont Institut ici
				</div>
			</div>
		</a>

	</div>
</div>

