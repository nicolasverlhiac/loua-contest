<div class="cols-row men">
	<div class="col-60 centered">
		<img src="<?php echo _INSTDIR_; ?>img/ld-content-men.jpg" alt="">
	</div>

	<div class="col-40">
		<h1>Laurence Dumont For men</h1>

		<p>Avec notre brigade 100% masculine, la parité est en marche! Fini le torse velu signe de virilité, les hommes aussi pourront dompter leur pilosité. Avec Laurence Dumont FOR MEN, ces messieurs ont des solutions pour des aisselles nettes, des jambes de cycliste, un dos lisse ou un torse impeccable pour le plus grand plaisir des yeux. </p>
		<p>Pour cela 2 armes redoutables sont à leur disposition : la crème dépilatoire Laurence Dumont For Men ou bien le disque à épiler. Parfait pour les plus douillets et les plus pressés!</p>

		
		<a href="http://laurence-dumont.fr/index.php?page=for-men&categorie=D&gamme=for%20men" target="_blank">
			<div class="cols-row ld-link">
				<div class="col-20">
					<img src="<?php echo _INSTDIR_; ?>img/ld-link.png" alt="">
				</div>
				<div class="col-80 men">
					Découvre plus en détail la gamme For Men ici
				</div>
			</div>
		</a>

	</div>
</div>