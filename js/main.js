$(document).ready(function () {



});


/* 
 * ADD New User
 */ 

$("form#inscription").submit(function() {

  var candidat_pseudo = $("form#inscription input[name='candidat_pseudo']").val();
  var candidat_mail = $("form#inscription input[name='candidat_mail']").val();
  var candidat_password = $("form#inscription input[name='candidat_password']").val();
  var candidat_type = $("form#inscription input[type=radio][name='candidat_type']:checked").val();


  $.ajax({
    type: "POST",
    url: "inc/functions.ajax.php",
    data: {
      "action":"addUser",
      "candidat_pseudo":candidat_pseudo,
      "candidat_mail":candidat_mail,
      "candidat_password":candidat_password,
      "candidat_type":candidat_type},
    success: function(msg){
      if (msg == "OK") {
        $("#report-erreur").html('<div class="info-box width-100"><p>Félicitation, votre compte est crée! </p> </div>');
        window.location = "http://loua.fr/post-legfie";
      } else {
        $("#report-erreur").html('<div class="danger-box width-100"><p>'+msg+'</p> </div>');
      }
    }
  });
  return false;
});


/* 
 * LOGIN
 */ 

$("form#login").submit(function() {

  var candidat_mail = $("form#login input[name='candidat_mail']").val();
  var candidat_password = $("form#login input[name='candidat_password']").val();


  $.ajax({
    type: "POST",
    url: "inc/functions.ajax.php",
    data: {
      "action":"login",
      "candidat_mail":candidat_mail,
      "candidat_password":candidat_password},
    success: function(msg){
      if (msg == "OK") {
        $("#report-erreur").html('<div class="info-box width-100"><p>Félicitation : On vous connecte </p> </div>');
        window.location = "http://loua.fr/legfies";
      } else {
        $("#report-erreur").html('<div class="danger-box width-100"><p>'+msg+'</p> </div>');
      }
    }
  });
  return false;
});



/* 
 * ADD Legfie
 */ 

$("form#addlegfie").submit(function() {

  var media_url = $("#crop-avatar .avatar-view img").attr('src');
  var media_description = $("#crop-avatar input[name='media_description']").val();
  var candidat_nom = $("form#addlegfie input[name='candidat_nom']").val();
  var candidat_prenom = $("form#addlegfie input[name='candidat_prenom']").val();
  var candidat_codepostal = $("form#addlegfie input[name='candidat_codepostal']").val();
  var candidat_birthday = $("form#addlegfie input[name='candidat_birthday']").val();

  $.ajax({
    type: "POST",
    url: "inc/functions.ajax.php",
    data: {
      "action":"addLegfie",
      "candidat_nom":candidat_nom,
      "candidat_prenom":candidat_prenom,
      "candidat_codepostal":candidat_codepostal,
      "candidat_birthday":candidat_birthday,
      "media_url":media_url},
    success: function(msg){
      if (msg == "OK") {
        $("#report-erreur").html('<div class="info-box width-100"><p><strong>Félicitations</strong> : votre legfie est en attente de modération. <br> Vous recevrez un mail lors de sa validation</p> </div>');
      } else {
        $("#report-erreur").html('<div class="danger-box width-100"><p>'+msg+'</p> </div>');
      }
    }
  });
  return false;
});


/* 
 * ADD Vote
 */ 

$("form#vote").submit(function() {

  var media_id = $("form#vote input[name='media_id']").val();

  $.ajax({
    type: "POST",
    url: "../inc/functions.ajax.php",
    data: {
      "action":"vote",
      "media_id":media_id},
    success: function(msg){
      if (msg == "OK") {
        $("#report-erreur").html('<p>Votre vote a été pris en compte !</p>');
        window.location.reload();
      } else {
        $("#report-erreur").html('<p>'+msg+'</p>');
      }
    }
  });
  return false;
});