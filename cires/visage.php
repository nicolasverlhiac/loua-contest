<div class="cols-row">
	<div class="col-40 centered">
		<img src="<?php echo _INSTDIR_; ?>img/bande-cire-visage.jpg" alt="">
	</div>

	<div class="col-60">
		<h1>12 bandes de cire froide <br> Visage </h1>

		<h2>Cas de délit de pilosité</h2>

		<p>Ton sourire ravageur est ton atout charme pour faire succomber tes prétendants ?</p>
		<p>Mais un ennemi redoutable s’est progressivement installé au-dessus de tes lèvres... Hors de question que tu perdes ton potentiel séduction à cause de ce petit criminel poilu coloré et disgracieux !</p>
		<p>L’agent LOUA spécial VISAGE est là pour te venir en aide…</p>

	</div>
</div>
<div class="cols-row">
	<div class="col-100">
		<h2>Intervention de l’agent LOUA spécial VISAGE  </h2>
		
		<p>Spécialement créées pour combattre nos ennemis les poils,  les 12 bandes de cire froide LOUA visage  sont l’allié idéal pour une épilation efficace et durable. Leur petit format est parfaitement adapté pour recouvrir entièrement ton duvet et le fera disparaître plus vite que ton ombre tout en respectant cette zone particulièrement délicate. </p>
		<p>Camouflées dans leur sachet nomade ultra fin, les bandes de cire froide LOUA se glisseront facilement dans ta valise, dans ton sac à main ou encore dans la poche de ton jean, et pourront être dégainées à tout instant en cas d’urgence pileuse non anticipée ou de flagrant délit de repousse !</p>
		
		<h2>Arrestation efficace et sans bavures</h2>

		<p>En quelques gestes simples, les bandes contribueront à l’élimination radicale de tes poils les plus rebelles. LOUA laisse ta peau lisse et douce pour te redonner un sourire de star digne de Shakira ! </p>
		<p>Grâce à leur efficacité sans faille, les bandes font régner la LOUA sur ton visage et t’assurent jusqu’à 4 semaines de tranquillité pileuse. Mission réussie ! </p>

		<h2>Zone d’intervention</h2>
		
		<p>Visage</p>

		<h2>Matériel à disposition </h2>

		<p>12 bandes de cire froide prêtes à l’action</p>
		<p>2 lingettes à l’huile d’azulène à la rescousse</p>
		<p>1 mode d’emploi pour une mission réussie</p>

		<h2>Caractéristiques</h2>

		<p>Nomades, discrètes, rapides et efficaces</p>

		<h2>Composition </h2>

		<p>BANDES </p>

		<p>INGREDIENTS : GLYCERYL ROSINATE, RICINUS COMMUNIS SEED OIL, CERA ALBA, PARFUM, PENTAERYTHRITYL TETRA-DI-T-BUTYL HYDROXYHYDROCINNAMATE, PRUNUS AMYGDALUS DULCIS OIL, ALOE BARBADENSIS LEAF EXTRACT, HELIANTHUS ANNUUS SEED OIL, TOCOPHERYL ACETATE, LINALOOL, BHT, ASCORBYL PALMITATE, CI 77891.</p>
		
		<p>LINGETTES</p>
		
		<p>INGREDIENTS : PARAFFINUM LIQUIDUM, ISOPROPYL MYRISTATE, TRITICUM VULGARE GERM OIL, PARFUM, PHENETHYL ALCOHOL, BHT, CAPRYLYL GLYCOL, GUAIAZULENE, ASCORBYL PALMITATE, CI61565.</p>

		<h2>Les règles de la peau lisse</h2>

		<p>Pour dompter nos ennemis poilus et faire régner la LOUA, à toi de jouer avec ton arme fatale !</p>
		<p>Avant la première utilisation, n’oublie pas de lire les précautions d’emploi sur la cartonnette à l’intérieur du sachet.</p>

		<ol>
			<li>
				<p><strong>Prépare le terrain en douceur :</strong> <br> Réuni tout ton matériel et fais-toi un espace beauté agréable et pratique. </p>
			</li>
			<li>
				<p><strong>Sors ton arme : </strong><br> Prend une double bande de cire froide et réchauffe-la quelques secondes entre tes mains. Sépare doucement et lentement les bandes. Il faut bien les réchauffer pour qu’elles se séparent facilement.	</p>
			</li>
			<li>
				<p><strong>Prépare-toi à intervenir : </strong><br> Applique l’une des deux bandes sur la zone que tu souhaites épiler. Lisse bien la bande dans le sens de la pousse du poil, pour être sûre de capturer le plus de poils possible. </p>
			</li>
			<li>
				<p><strong>Moment fatidique : le retrait de la bande</strong><br>D’une main, tend bien ta peau et de l’autre prépare-toi à retirer la bande d’un geste vif dans le sens INVERSE de la pousse du poil et en étant bien parallèle à ta peau. Prend une grande inspiration… Tire ! Recommence sur une autre zone jusqu’à ce que la bande ne colle plus, puis change de bande.</p>
			</li>
			<li>
				<p><strong>Arrestation sans bavure </strong><br> Utilise la lingette à l’huile d’azulène pour t’aider à enlever les résidus de cire (ça ne part pas avec de l’eau ou du savon).</p>
			</li>
			<li>
				<p><strong>Bienvenue dans la peau lisse ! </strong><br> Tu as fini sans t’évanouir? Mission réussie, tu es désormais une vraie tueuse (de poils) ! </p>
			</li>
		</ol>
	
	</div>
</div>