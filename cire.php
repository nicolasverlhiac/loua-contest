<?php 
include_once ("inc/config.php");

$key = $_GET["key"];

switch ($key) {
	case 'visage':
		$titre = "visage";
		$description = "Ton sourire ravageur est ton atout charme pour faire succomber tes prétendants ? Mais un ennemi redoutable s’est progressivement installé au-dessus de tes lèvres... Hors de question que tu perdes ton potentiel séduction à cause de ce petit criminel poilu coloré et disgracieux !";
		$poil = "rebelle";
		$metaTitle = "12 Bandes épilation pour le Visage";
		break;

	case 'aisselles':
		$titre = "aisselles";
		$description = "C’est le branle-bas de combat sous tes bras ? Il est temps d’intervenir sur le champ de bataille contre ces guerriers poilus qui gagnent du terrain !";
		$poil = "dru";
		$metaTitle = "12 Bandes épilation pour les Aisselles";
		break;

	case 'jambes':
		$titre = "jambes";
		$description = "Ton copain se plaint de passer ses vacances avec un yéti ? Il est temps de quitter cette parure qui te protégeait du froid et redonner à tes jambes de rêve le pouvoir de porter tes robes préférées tout au long de l’été !";
		$poil = "frise";
		$metaTitle = "12 Bandes épilation pour les Jambes";
		break;
	
	case 'maillot':
		$titre = "maillot";
		$description = "L’été arrive et tu aimerais sortir ton petit itsi bitsi tini ouini, tout petit, petit bikini ? Mais tels de mauvaises herbes, tes poils se sont progressivement répandus sur cette zone qui s’est transformée en jungle tropicale... Il est temps de reprendre le contrôle sur ces poils indésirables et disgracieux !";
		$poil = "incarne";
		$metaTitle = "12 Bandes épilation pour le Maillot";
		break;

	default:
		$titre = "What is the fuck";
		break;
}

?>
<!DOCTYPE html>
<!--[if lt IE 7]><html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]><html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]> <html class="no-js lt-ie9"><![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js" lang="fr"> <!--<![endif]-->
<!--[if lt IE 9]><script src="//html5shiv.googlecode.com/svn/trunk/html5.js"></script><![endif]-->
<html lang="fr">

	<head>
		<meta charset="utf-8" />
		<title><?php echo $metaTitle; ?></title>

		<meta http-equiv="content-type" content="text/html; charset=utf-8" />
		<meta name="description" content="<?php echo $description; ?>" />
		<meta name="viewport" content="initial-scale=1,minimum-scale=1,width=device-width">

		<!-- Open Graph facebook -->
		<meta property="og:url" content="http://"/>
		<meta property="og:title" content=""/> 
		<meta property="og:type" content="website"/> 
		<meta property="og:image" content="http://"/> 
		<meta property="og:site_name" content=""/> 
		<meta property="og:description" content=""/>
		<meta name="viewport" content="initial-scale=1,minimum-scale=1,width=device-width">

		<?php include_once("header.php"); ?>

			<div class="content">
				<div class="menu-bandes cols-row">

					<div class="col-70 col-centre">
						<div class="cols-row end begin">
							<div class="col-25">
								<a href="<?php echo _INSTDIR_; ?>nos-bandes/bande-cire-froide-visage">
									<img src="<?php echo _INSTDIR_; ?>img/bande-cire-visage.jpg" alt="">
								</a>
							</div>
							<div class="col-25">
								<a href="<?php echo _INSTDIR_; ?>nos-bandes/bande-cire-froide-aisselles">
									<img src="<?php echo _INSTDIR_; ?>img/bande-cire-aisselles.jpg" alt="">
								</a>
							</div>
							<div class="col-25">
								<a href="<?php echo _INSTDIR_; ?>nos-bandes/bande-cire-froide-maillot">
									<img src="<?php echo _INSTDIR_; ?>img/bande-cire-maillot.jpg" alt="">
								</a>
							</div>
							<div class="col-25">
								<a href="<?php echo _INSTDIR_; ?>nos-bandes/bande-cire-froide-jambes">
									<img src="<?php echo _INSTDIR_; ?>img/bande-cire-jambes.jpg" alt="">
								</a>
							</div>	
						</div>
					</div>
				</div>
				<div class="cols-row">
					<div class="col-66 bande-content">

						<?php include_once("cires/$titre.php") ; ?>

					</div>

					<div class="col-33">
						<a href="<?php echo _INSTDIR_; ?>most-wanted/poil-<?php echo $poil; ?>">
							<img src="<?php echo _INSTDIR_; ?>img/sidebar-<?php echo $titre; ?>.png" alt="">
						</a>
					</div>
				</div>

				<?php include_once("footer.php"); ?>
			</div>
			
		</div>
		
		<!-- <script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script> -->
		<script src="<?php echo _INSTDIR_; ?>js/jquery.min.js"></script>
		<script src="<?php echo _INSTDIR_; ?>js/main.js"></script>

	</body>
</html>