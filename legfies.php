<?php 
require_once ("inc/config.php");
require_once ("inc/class.contest.php");
require_once ("inc/functions.php");

$contest = new Contest();

$key = $_GET["key"];

if ($key == "index.html") {
	echo "Index";
}

$curentUrl = $_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI'];

// Get a legfie
$leggfie = $contest->displayLegfie($key);

$likes = $leggfie[0]["media_likes"];

$userId = $leggfie[0]["user_id"];
$user = $contest->displayLegfieInfo($userId);

?>
<!DOCTYPE html>
<!--[if lt IE 7]><html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]><html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]> <html class="no-js lt-ie9"><![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js" lang="fr"> <!--<![endif]-->
<!--[if lt IE 9]><script src="//html5shiv.googlecode.com/svn/trunk/html5.js"></script><![endif]-->
<html lang="fr">

	<head>
		<meta charset="utf-8" />
		<title>Legfies de <?php echo $user[0]["user_login"]; ?> - Loua</title>

		<meta http-equiv="content-type" content="text/html; charset=utf-8" />
		<meta name="description" content="Participe à l’élection des plus beaux Legfies et tente de gagner 1 voyage pour 2 aux Antilles  et plein d’autres cadeaux! " />
		<meta name="viewport" content="initial-scale=1,minimum-scale=1,width=device-width">

		<!-- Open Graph facebook -->
		<meta property="og:url" content="http://loua.fr/legfies/<?php echo $key; ?>"/>
		<meta property="og:title" content="Jeu concours LOUA "/> 
		<meta property="og:type" content="website"/> 
		<meta property="og:image" content="http://loua.fr<?php echo _INSTDIR_.$leggfie[0]["media_url"]; ?>"/> 
		<meta property="og:site_name" content="LOUA.fr"/> 
		<meta property="og:description" content="Participe à l’élection des plus beaux Legfies et tente de gagner 1 voyage pour 2 aux Antilles  et plein d’autres cadeaux! "/>
		<meta name="viewport" content="initial-scale=1,minimum-scale=1,width=device-width">

		<?php include_once("header.php"); ?>

			<div class="content">

				<div class="cols-row">
					<div class="col-80 col-centre">
						<div class="cols-row centered menu-game">
							<div class="col-50">
								<a href="<?php echo _INSTDIR_; ?>legfies" class="bleu">
									Retour à la <br>
									galerie des legfies
								</a>
							</div>
							<div class="col-33">
								<a href="<?php echo _INSTDIR_; ?>post-legfie" class="orange">
									Je poste<br>
									mon legfie
								</a>
							</div>
						</div>
					</div>
				</div>
				<?php if ($leggfie[0]["media_status"] == 1): ?>
					
				<div class="cols-row">
					<div class="col-50 centered legfie-img">
							<img src="<?php echo _INSTDIR_.$leggfie[0]["media_url"]; ?>" alt="">
					</div>
					<div class="col-50 likes">
						<h3><?php echo $user[0]["user_login"]; ?></h3>
						<?php 
							if (!isset($likes)) {
								echo "<p> Pas de Likes pour le moment. </p>";
							}
							elseif ($likes == 1){
								echo "<p><strong>1</strong> like </p>";
							} else {
						 ?>
						<p><strong><?php echo $likes; ?></strong> likes</p>
						<?php } ?>
						
						<?php if (isset($_SESSION['loginId'])){ ?>
						<form id="vote" action="" method="POST">
							
							<input type="hidden" name="media_id" value="<?php echo $key; ?>">
							<div class="cols-row end begin">
								<div class="col-25">
									<img src="<?php echo _INSTDIR_; ?>img/like-btn.png" alt="">
								</div>
								<div class="col-75">
									<input type="submit" class="btn-form" value="+1 vote">	
								</div>
							</div>

							<div id="report-erreur"></div>

						</form>
						<?php } else {?>
							<div class="width-100">
								<p><strong>Il faut être connecté pour voter !</strong></p>
							</div>
							<div class="cols-row">
								<div class="col-40">
									<a class="btn-form" href="<?php echo _INSTDIR_; ?>login">Connexion</a>
								</div>
								<div class="col-10">
									ou
								</div>
								<div class="col-40">
									<a class="btn-form" href="<?php echo _INSTDIR_; ?>jeux">Inscription</a>
								</div>
							</div>
						<?php } ?>

						<!-- Btn RS -->
						<div class="share-btn">
					        <a target="_blank" title="Facebook" href="https://www.facebook.com/sharer.php?u=<?php echo $curentUrl; ?>&t=" rel="nofollow" onclick="javascript:window.open(this.href, '', 'menubar=no,toolbar=no,resizable=yes,scrollbars=yes,height=500,width=700');return false;">
					        	<img src="<?php echo _INSTDIR_; ?>img/share-fb.png" alt="Twitter" />
					        </a>
					        <a target="_blank" title="Twitter" href="https://twitter.com/share?url=<?php echo $curentUrl; ?>&text=&via=" rel="nofollow" onclick="javascript:window.open(this.href, '', 'menubar=no,toolbar=no,resizable=yes,scrollbars=yes,height=400,width=700');return false;">
					        	<img src="<?php echo _INSTDIR_; ?>img/share-tw.png" alt="Facebook" />
					        </a>
						</div>

					</div>
				</div>
				<?php endif ?>
				<?php if ($likes = $leggfie[0]["media_status"] == 0): ?>
					<div class="cols-row">
						<div class="col-100 centered">
							<h2 class="pink">En attente de validation ..</h2>	
						</div>
					</div>
				<?php endif ?>


				<?php if ($likes = $leggfie[0]["media_status"] == 2): ?>
					<div class="cols-row">
						<div class="col-100 centered">
							<h2 class="pink">Ce legfie a été supprimé ..</h2>	
						</div>
					</div>
				<?php endif ?>


				<?php include_once("footer.php"); ?>
			</div>
			
		</div>
		
		<!-- <script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script> -->
		<script src="<?php echo _INSTDIR_; ?>js/jquery.min.js"></script>
		<script src="<?php echo _INSTDIR_; ?>js/main.js"></script>

	</body>
</html>