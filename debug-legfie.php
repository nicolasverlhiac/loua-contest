<?php 
require_once ("inc/config.php");
require_once ("inc/class.contest.php");
require_once ("inc/functions.php");

$contest = new Contest();


?>


<!DOCTYPE html>
<!--[if lt IE 7]><html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]><html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]> <html class="no-js lt-ie9"><![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js" lang="fr"> <!--<![endif]-->
<!--[if lt IE 9]><script src="//html5shiv.googlecode.com/svn/trunk/html5.js"></script><![endif]-->
<html lang="fr">

	<head>
		<meta charset="utf-8" />
		<title>Debug</title>

		<meta http-equiv="content-type" content="text/html; charset=utf-8" />
		<meta name="viewport" content="initial-scale=1,minimum-scale=1,width=device-width">

		<!-- Open Graph facebook -->
		<meta property="og:url" content="http://"/>
		<meta property="og:title" content=""/> 
		<meta property="og:type" content="website"/> 
		<meta property="og:image" content="http://"/> 
		<meta property="og:site_name" content=""/> 
		<meta property="og:description" content=""/>
		<meta name="viewport" content="initial-scale=1,minimum-scale=1,width=device-width">

		<link href="<?php echo _INSTDIR_; ?>crop/css/bootstrap.min.css" rel="stylesheet">
		<link href="<?php echo _INSTDIR_; ?>crop/css/cropper.min.css" rel="stylesheet">
		<link href="<?php echo _INSTDIR_; ?>crop/css/main.css" rel="stylesheet">

		<?php include_once("header.php"); ?>

			<div class="content">
				<div class="cols-row">

					<div class="col-50">

						<div class="" id="crop-avatar">

						    <!-- Current avatar -->
						    <div class="avatar-view" title="Choisissez votre legfie">
						    <?php if (!isset($_SESSION['login'])) { ?>
						    	<img src="<?php echo _INSTDIR_; ?>img/legfie.jpg" alt="Avatar">
						    <?php } else { ?>
								<img src="<?php echo _INSTDIR_; ?>img/legfie.jpg" alt="Avatar">
						    <?php }?>
						    </div>

						    <!-- Cropping modal -->
						    <div class="modal fade" id="avatar-modal" aria-hidden="true" aria-labelledby="avatar-modal-label" role="dialog" tabindex="-1">
						      <div class="modal-dialog modal-lg">
						        <div class="modal-content">
						          <form class="avatar-form" action="<?php echo _INSTDIR_; ?>class.crop.php" enctype="multipart/form-data" method="post">
						            <div class="modal-header">
						              <button class="close" data-dismiss="modal" type="button">&times;</button>
						              <h4 class="modal-title" id="avatar-modal-label">Envoyez votre leggfie</h4>
						            </div>
						            <div class="modal-body">
						              <div class="avatar-body">

						                <!-- Upload image and data -->
						                <div class="avatar-upload">
						                  <input class="avatar-src" name="avatar_src" type="hidden" value="http://localhost:8888/loua/contest-app/data/24/20150630181111.original.jpeg">
						                  <input class="avatar-data" name="avatar_data" type="hidden">
						                  <label for="avatarInput">Uploadez votre image</label>
						                  <input class="avatar-input" id="avatarInput" name="avatar_file" type="file">
						                </div>



						                <!-- Crop and preview -->
						                <div class="row">
						                  <div class="col-md-9">
						                    <div class="avatar-wrapper"></div>
						                  </div>
						                  <div class="col-md-3">
						                    <div class="avatar-preview preview-lg"></div>
						                    <div class="avatar-preview preview-md"></div>
						                    <div class="avatar-preview preview-sm"></div>
						                  </div>
						                </div>

						                <div class="row avatar-btns">
						                  <div class="col-md-9">
						                    <div class="btn-group">
						                      <button class="btn btn-primary" data-method="rotate" data-option="-90" type="button" title="Rotate -90 degrees">Rotation Gauche</button>
						                      <button class="btn btn-primary" data-method="rotate" data-option="-15" type="button">-15deg</button>
						                      <button class="btn btn-primary" data-method="rotate" data-option="-30" type="button">-30deg</button>
						                      <button class="btn btn-primary" data-method="rotate" data-option="-45" type="button">-45deg</button>
						                    </div>
						                    <div class="btn-group">
						                      <button class="btn btn-primary" data-method="rotate" data-option="90" type="button" title="Rotate 90 degrees">Rotation Droite</button>
						                      <button class="btn btn-primary" data-method="rotate" data-option="15" type="button">15deg</button>
						                      <button class="btn btn-primary" data-method="rotate" data-option="30" type="button">30deg</button>
						                      <button class="btn btn-primary" data-method="rotate" data-option="45" type="button">45deg</button>
						                    </div>
						                  </div>
						                  <div class="col-md-3">
						                    <button class="btn btn-primary btn-block avatar-save" type="submit">Envoyer</button>
						                  </div>
						                </div>
						              </div>
						            </div>
						            <!-- <div class="modal-footer">
						              <button class="btn btn-default" data-dismiss="modal" type="button">Close</button>
						            </div> -->
						          </form>
						        </div>
						      </div>
						    </div><!-- /.modal -->

						    <!-- Loading state -->
						    <div class="loading" aria-label="Loading" role="img" tabindex="-1"></div>
						  </div>

					</div>
				


				<?php include_once("footer.php"); ?>
			</div>
			
		</div>
		
		<script src="<?php echo _INSTDIR_; ?>crop/js/jquery.min.js"></script>
		<script src="<?php echo _INSTDIR_; ?>crop/js/bootstrap.min.js"></script>
		<script src="<?php echo _INSTDIR_; ?>crop/js/cropper.min.js"></script>
		<script src="<?php echo _INSTDIR_; ?>crop/js/main-debug.js"></script>

		<script src="<?php echo _INSTDIR_; ?>js/main.js"></script>

	</body>
</html>