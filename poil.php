<?php 
include_once ("inc/config.php");

$key = $_GET["key"];

switch ($key) {
	case 'dru':
		$titre = "dru";
		$image = "aisselles";
		$metaTitle = "Poil Dru - Most Wanted";
		$metaDesc = "Poil Dru, recherché par la peau lisse! Découvrez son arrestation par la LOUA dans les vidéos.";
		break;

	case 'rebelle':
		$titre = "rebelle";
		$image = "visage";
		$metaTitle = "Poil Rebelle - Most Wanted";
		$metaDesc = "Poil Rebelle, recherché par la peau lisse! Découvrez son arrestation par la LOUA dans les vidéos.";
		break;

	case 'incarne':
		$titre = "incarne";
		$image = "maillot";
		$metaTitle = "Poil Incarné - Most Wanted";
		$metaDesc = "Poil Incarné, recherché par la peau lisse! Découvrez son arrestation par la LOUA dans les vidéos.";
		break;
	
	case 'frise':
		$titre = "frise";
		$image = "jambes";
		$metaTitle = "Poil Frisé - Most Wanted";
		$metaDesc = "Poil Frisé, recherché par la peau lisse! Découvrez son arrestation par la LOUA dans les vidéos.";
		break;

	default:
		$titre = "What is the fuck";
		break;
}

?>
<!DOCTYPE html>
<!--[if lt IE 7]><html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]><html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]> <html class="no-js lt-ie9"><![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js" lang="fr"> <!--<![endif]-->
<!--[if lt IE 9]><script src="//html5shiv.googlecode.com/svn/trunk/html5.js"></script><![endif]-->
<html lang="fr">

	<head>
		<meta charset="utf-8" />
		<title><?php echo $metaTitle; ?></title>

		<meta http-equiv="content-type" content="text/html; charset=utf-8" />
		<meta name="description" content="<?php echo $metaDesc; ?>" />
		<meta name="viewport" content="initial-scale=1,minimum-scale=1,width=device-width">

		<!-- Open Graph facebook -->
		<meta property="og:url" content="http://"/>
		<meta property="og:title" content=""/> 
		<meta property="og:type" content="website"/> 
		<meta property="og:image" content="http://"/> 
		<meta property="og:site_name" content=""/> 
		<meta property="og:description" content=""/>	
		<meta name="viewport" content="initial-scale=1,minimum-scale=1,width=device-width">

		<?php include_once("header.php"); ?>

			<div class="content">
				<div class="menu-bandes menu-poil rounded cols-row">
					<div class="col-90 cols-split col-centre">
						<div class="cols-row end begin">
							<div class="col-25">
								<a href="<?php echo _INSTDIR_; ?>most-wanted/poil-rebelle">
									<img src="<?php echo _INSTDIR_; ?>img/poil-rebelle.png" alt="">
								</a>
							</div>
							<div class="col-25">
								<a href="<?php echo _INSTDIR_; ?>most-wanted/poil-dru">
									<img src="<?php echo _INSTDIR_; ?>img/poil-dru.png" alt="">
								</a>
							</div>
							<div class="col-25">
								<a href="<?php echo _INSTDIR_; ?>most-wanted/poil-frise">
									<img src="<?php echo _INSTDIR_; ?>img/poil-frise.png" alt="">
								</a>
							</div>
							<div class="col-25">
								<a href="<?php echo _INSTDIR_; ?>most-wanted/poil-incarne">
									<img src="<?php echo _INSTDIR_; ?>img/poil-incarne.png" alt="">
								</a>
							</div>	
						</div>
					</div>
				</div>

				<div class="cols-row poil-content poil-<?php echo $titre; ?>">
					<div class="col-33">
						<img src="<?php echo _INSTDIR_; ?>img/sidebar-<?php echo $image; ?>.png" alt="">		
					</div>
					<div class="col-66">
						<?php include_once("poils/".$titre.".php"); ?>
					</div>
				</div>

				<?php include_once("footer.php"); ?>
			</div>
			
		</div>
		
		<!-- <script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script> -->
		<script src="<?php echo _INSTDIR_; ?>js/jquery.min.js"></script>
		<script src="<?php echo _INSTDIR_; ?>js/main.js"></script>

	</body>
</html>