<?php 
include_once ("inc/config.php");
require_once ("inc/class.contest.php");
require_once ("inc/functions.php");

$contest = new Contest();



$legfies = $contest->displayLastLegfie();


?>
<!DOCTYPE html>
<!--[if lt IE 7]><html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]><html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]> <html class="no-js lt-ie9"><![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js" lang="fr"> <!--<![endif]-->
<!--[if lt IE 9]><script src="//html5shiv.googlecode.com/svn/trunk/html5.js"></script><![endif]-->
<html lang="fr">

	<head>
		<meta charset="utf-8" />
		<title>Jeux Concours Loua</title>

		<meta http-equiv="content-type" content="text/html; charset=utf-8" />
		<meta name="description" content="Envoyez votre Legfie pour participer au grand jeux concours LOUA de l'été 2015. " />
		<meta name="viewport" content="initial-scale=1,minimum-scale=1,width=device-width">

		<!-- Open Graph facebook -->
		<meta property="og:url" content="http://"/>
		<meta property="og:title" content=""/> 
		<meta property="og:type" content="website"/> 
		<meta property="og:image" content="http://"/> 
		<meta property="og:site_name" content=""/> 
		<meta property="og:description" content=""/>
		<meta name="viewport" content="initial-scale=1,minimum-scale=1,width=device-width">

		<?php include_once("header.php"); ?>

			<div class="content">
				
				<?php include("g-header.php"); ?>
				<div class="cols-row">
					<div class="col-100">
						<img class="game-slider" src="<?php echo _INSTDIR_; ?>img/game-slider.png" alt="">	
					</div>
				</div>
				<div class="cols-row">
					<div class="col-50">
						<img src="<?php echo _INSTDIR_; ?>img/game-step.jpg" alt="">
					</div>
					<div class="col-50">
						<div class="cols-row end begin game-form">

							<?php if (!isset($_SESSION['loginId'])){ ?>
								<div class="col-100  col-centre">
									<h2>Formulaire d'inscription</h2>
									<form id="inscription" action="" method="POST">

										<input id="pseudo" name="candidat_pseudo" class="input width-100" placeholder="Pseudo" type="text">

										<input id="mail" name="candidat_mail" class="input width-100" placeholder="Adresse e-mail" type="email">

										<input id="psw" name="candidat_password" class="input width-100" placeholder="Mot de passe" type="password">

										<div class="group-input">
												<label>
													<input type="radio" name="candidat_type" value="2" checked> Femme
												</label>

												<label>
													<input type="radio" name="candidat_type" value="1" > Homme
												</label>
											</div>

										<input type="submit" class="btn-form width-100" value="Inscription">
										<div id="report-erreur"></div>

									</form>	
								</div>
							<?php } else { ?>
								<a href="<?php echo _INSTDIR_; ?>post-legfie" class="centered btn-form width-100"> Poste ton Legfie</a>
							<?php } ?>
						</div>
					</div>
				</div>
				<div class="cols-row">

					<?php foreach ($legfies as $legfie): ?>
					<div class="col-20 legfie-img-2">
						<a href="<?php echo _INSTDIR_."legfies/".$legfie->media_id ?>">
							<img src="<?php echo $legfie->media_url?>" alt=""/>	
						</a>
					</div>
					<?php endforeach; ?>

				</div>

				<div class="cols-row end">
					<div class="col-100 centered pink">
						<p>Un tirage au sort sera effectué parmi tous les participants du legfie. Le gagnant remportera un VOYAGE pour deux personnes aux ANTILLES !
						<br>Ta photo n’a pas été tirée au sort ? Rien n’est perdu ! Les 50 legfies ayant obtenus le plus de votes remporteront de nombreux CADEAUX !</p>
						<p><strong>Annonce des gagnants le 7 septembre </strong></p>
						<p>Pour plus d'informations, consulte le <a href="<?php echo _INSTDIR_; ?>reglement-jeux-concours">règlement du jeu-concours.</a></p>

					</div>
				</div>
				
				<div class="cols-row">
					<div class="col-100 centered">
						<a href="http://loua.fr/lots-a-gagner" class="btn-form">DÉCOUVRE VITE LES CADEAUX À GAGNER !</a>
					</div>
				</div>
				<div class="cols-row begin">
					<div class="col-100">
						<a href="<?php echo _INSTDIR_; ?>lots-a-gagner">
							<img src="<?php echo _INSTDIR_; ?>img/game-footer.jpg" alt="">
						</a>
					</div>
				</div>

				<?php include_once("footer.php"); ?>
			</div>
			
		</div>
		
		<!-- <script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script> -->
		<script src="<?php echo _INSTDIR_; ?>js/jquery.min.js"></script>
		<script src="<?php echo _INSTDIR_; ?>js/main.js"></script>

	</body>
</html>