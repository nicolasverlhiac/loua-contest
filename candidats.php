<?php 
require_once ("inc/config.php");
require_once ("inc/class.contest.php");
require_once ("inc/functions.php");

$contest = new Contest();

$id = $_GET["id"];

// Information de l'utilisateur
$user = $contest->displayLegfieInfo($id);
$media = $contest->displayMedia($id);

?>

<!DOCTYPE html>
<!--[if lt IE 7]><html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]><html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]> <html class="no-js lt-ie9"><![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js" lang="fr"> <!--<![endif]-->
<!--[if lt IE 9]><script src="//html5shiv.googlecode.com/svn/trunk/html5.js"></script><![endif]-->
<html lang="fr">

	<head>
		<meta charset="utf-8" />
		<title>Legfies de <?php echo $user[0]["user_login"]; ?> - Loua</title>

		<meta http-equiv="content-type" content="text/html; charset=utf-8" />
		<meta name="description" content="Participe à l’élection des plus beaux Legfies et tente de gagner 1 voyage pour 2 aux Antilles  et plein d’autres cadeaux! " />
		<meta name="viewport" content="initial-scale=1,minimum-scale=1,width=device-width">

		<?php include_once("header.php"); ?>

			<div class="content">

				<div class="cols-row">
					<div class="col-50 centered legfie-img">
							<img src="<?php echo "http://loua.fr/".$media[0]["media_url"]; ?>" alt="">
					</div>
					<div class="col-50 likes">
						<h3><?php echo $user[0]["user_login"]; ?> / <?php echo $media[0]["media_id"]; ?></h3>
						<p><?php echo $media[0]["media_likes"]; ?> Likes </p>
						<p><strong><?php echo $user[0]["user_email"]; ?></strong></p>
						<p><?php echo $user[0]["user_prenom"]; ?> <?php echo $user[0]["user_name"]; ?></p>
						<p><?php echo $user[0]["user_codepostal"]; ?></p>
						<p><?php echo $user[0]["user_registered"]; ?></p>
						<p><?php echo $user[0]["user_ip"]; ?></p>
					</div>
				</div>
				<?php include_once("footer.php"); ?>
			</div>
			
		</div>
		
		<!-- <script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script> -->
		<script src="<?php echo _INSTDIR_; ?>js/jquery.min.js"></script>
		<script src="<?php echo _INSTDIR_; ?>js/main.js"></script>

	</body>
</html>