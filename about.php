<?php 
include_once ("inc/config.php");


?>
<!DOCTYPE html>
<!--[if lt IE 7]><html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]><html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]> <html class="no-js lt-ie9"><![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js" lang="fr"> <!--<![endif]-->
<!--[if lt IE 9]><script src="//html5shiv.googlecode.com/svn/trunk/html5.js"></script><![endif]-->
<html lang="fr">

	<head>
		<meta charset="utf-8" />
		<title>Connaître la LOUA</title>

		<meta http-equiv="content-type" content="text/html; charset=utf-8" />
		<meta name="description" content="LOUA, c’est la nouvelle Brigade Anti-Poils qui va contribuer à la lutte contre la sur-pilosité." />
		<meta name="viewport" content="initial-scale=1,minimum-scale=1,width=device-width">

		<!-- Open Graph facebook -->
		<meta property="og:url" content="http://"/>
		<meta property="og:title" content=""/> 
		<meta property="og:type" content="website"/> 
		<meta property="og:image" content="http://"/> 
		<meta property="og:site_name" content=""/> 
		<meta property="og:description" content=""/>
		<meta name="viewport" content="initial-scale=1,minimum-scale=1,width=device-width">

		<?php include_once("header.php"); ?>

			<div class="content">
				<img src="<?php echo _INSTDIR_; ?>img/la-loua.jpg" alt="">
				<div class="cols-row about-content">
					<div class="col-66">
						<h1>LOUA, c’est QUOUA ? </h1>

						<p>LOUA, c’est la nouvelle Brigade Anti-Poils qui va contribuer à la lutte contre la sur-pilosité qui sévit sur notre planète ! Notre mission : punir les délinquants pileux multi récidivistes...</p>
						<p>Spécialement créées pour combattre nos ennemis les poils,  les bandes de cire froide LOUA  sont l’allié idéal pour une épilation efficace et durable.</p>
						<p>En quelques minutes, LOUA contribuera à l’élimination radicale de tes poils les plus rebelles. LOUA laisse ta peau lisse et douce pour libérer la femme fatale qui est en toi.</p>
						<p>Grâce à leur efficacité sans faille, les bandes font régner la LOUA et t’assurent jusqu’à 4 semaines de tranquillité pileuse : la peau lisse, c’est TOUA !</p>

						<h2>Contrôle d’épilation, vous êtes en état d’arrestation !</h2>

						<p>Rien ne l’arrête, qu’il fasse froid, sec ou que tu sois en terre tropicale, la brigade LOUA se tient prête à intervenir pour toi. </p>
						<p>Camouflées dans leur sachet nomade ultra fin, les bandes de cire froide LOUA te suivront partout dans ta quête de peau lisse. En les glissant dans ta valise, dans ton sac à main ou encore dans la poche de ton jean, tu pourras les dégainer à tout instant en cas d’urgence pileuse non anticipée ou de flagrant délit de repousse.</p>
						<p>Accessoire désormais indispensable pour partir en vacances, elles se glisseront facilement entre ta brosse à dents, ton maillot de bain et ta crème solaire.  Un détail primordial pour pouvoir porter fièrement tes tenues estivales favorites ! </p>


						<h2>Peau lisse, haut les jambes !</h2>

						<p>Pas de répit possible pour les poils naissants qui pointent le bout de leur nez au milieu des vacances.. Ton gros pot de cire encombrant et tout ton attirail épilation sont restés à la maison ? Pas de panique ! L’efficacité redoutable des bandes LOUA dans leur pochette 100% nomade permet une épilation parfaite à portée de main, ou des petites retouches ponctuelles en un éclair. </p>
						<p>Avec 12 bandes dans chaque sachet, il y a largement de quoi traquer l’ennemi jusque dans les moindres recoins. Ta meilleure arme beauté est désormais compacte et discrète, tu peux la dégainer n’importe où, n’importe quand : effet de surprise garanti pour l’arrestation de ces délinquants poilus !</p>
						<p>Pratiques et efficaces, les bandes de cire froide LOUA te permettent de condamner les poils tout en douceur et en respectant ta peau. Formulées avec de l’huile d’Amande douce et de l’Aloe Vera, reconnues pour leurs propriétés apaisantes, ta peau est préservée pendant l’épilation. Cerise sur le gâteau, leur parfum gourmand à la vanille te fera succomber de plaisir et tu ne pourras plus jamais t’en passer ! </p>

						<h2>Brigade Anti Poils prête à l’action </h2>

						<p>Notre unité d’élite est composée de 4 agents d’épilation très très spéciaux. La brigade LOUA réplique aux violentes attaques de poils clandestins sur toutes les zones : </p>
						<ul>
							<li>Le visage : adieu duvet disgracieux bonjour sourire de star digne de Shakira.. Prête à faire succomber tes prétendants ?</li>
							<li>Les aisselles : Bustiers, débardeurs ? Plus rien ne te fait peur !! </li>
							<li>Le maillot : ne craint plus de sortir ton petit itsi bitsi tini ouini, tout petit, petit bikini ! A toi le bronzage parfait sur la plage !</li>
							<li>Les jambes : pour ton copain c’est fini les vacances avec le yéti, tes jambes de rêves porteront dignement tes robes préférées tout au long de l’été !</li>
						</ul>

						<h2>Pour qui ?</h2>

						<p>Que tu aies le poil ras, le poil long, le poil lisse, le poil soyeux, le poil frisé ou encore le poil dru, LOUA est fait pour TOUA !  </p>
						<p>Que tu aies un système pileux surdéveloppé ou seulement 2 -3 poils en cavale, LOUA est fait pour TOUA !</p>
						<p>Que tu sois douillette, insensible ou que tu n’aies peur de rien, LOUA est fait pour TOUA ! </p>
						<p>Que ta peau soit sèche, sensible ou normale, LOUA est fait pour TOUA !</p>
						<p>Que tu sois novice ou experte dans l’extermination de poils, LOUA est fait pour TOUA !</p>
						<p>Bref, LOUA c’est LA solution épilation adaptée à tous les types de peau et toutes les utilisatrices. </p>



					</div>
					<div class="col-33">
						<?php include_once("sidebar.php") ?>
					</div>

				</div>
				<?php include_once("footer.php"); ?>
			</div>
			
		</div>
		
		<!-- <script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script> -->
		<script src="<?php echo _INSTDIR_; ?>js/jquery.min.js"></script>
		<script src="<?php echo _INSTDIR_; ?>js/main.js"></script>

	</body>
</html>