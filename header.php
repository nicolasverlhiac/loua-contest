		<!-- Stylesheets -->
		<link rel='stylesheet' href='http://fonts.googleapis.com/css?family=Open+Sans:400,700' type='text/css' media='all' />
		<link rel="stylesheet" href="<?php echo _INSTDIR_; ?>css/slim-icons.min.css" type="text/css" media="screen" />
		<link rel="stylesheet" href="<?php echo _INSTDIR_; ?>css/slim.min.css" type="text/css" media="screen" />
		<link rel="stylesheet" href="<?php echo _INSTDIR_; ?>css/main.css" type="text/css" media="screen" />
		<link rel="icon" href="<?php echo _INSTDIR_; ?>favicon.ico" />
	</head>

	<?php // include_once ("inc/debug.php"); ?>

	<body class="bg-1">

		<div class="container">
			
			<div class="header">

				<a class="instant" href="https://www.facebook.com/LaurenceDumontCosmetiques?app_data=%7B%7D&v=app_160812950681114" target="_blank"></a>
				
				<div class="cols-row cols-split begin end">
					<div class="col-40 centered col-centre">
						<a class="logo-home" href="<?php echo _INSTDIR_; ?>">
							<img src="<?php echo _INSTDIR_; ?>img/logo-loua.png" alt="">
						</a>
						<a href="<?php echo _INSTDIR_; ?>" class="btn-form btn-acc">Accueil</a>
					</div>
				</div>	
			

			</div>

			<nav class="navbar full main-menu">	
				<ul>
					<li class="<?php if($f=='cire.php' ) echo 'current';?>"><a href="<?php echo _INSTDIR_; ?>nos-bandes/bande-cire-froide-jambes">Nos Bandes</a></li>
					<li class="<?php if($f=='about.php' ) echo 'current';?>"><a href="<?php echo _INSTDIR_; ?>connaitre-la-loua">Connaître la Loua</a></li>
					<li class="<?php if($f=='poil.php' ) echo 'current';?>"><a href="<?php echo _INSTDIR_; ?>most-wanted/poil-dru">Recherchés par la peau lisse</a></li>
					<li class="<?php if($f=='' ) echo 'current';?>"><a href="<?php echo _INSTDIR_; ?>elles-font-respecter-la-loua">Elles font respecter la LOUA</a></li>
					<li class="<?php if($f=='special-unit.php' ) echo 'current';?>"><a href="<?php echo _INSTDIR_; ?>nos-unites-speciales/laurence-dumont-institut">Nos unités spéciales</a></li>
					<li class="<?php if($f=='' ) echo 'current';?>"><a href="<?php echo _INSTDIR_; ?>jeux">Jeux</a></li>
				</ul>
			</nav>